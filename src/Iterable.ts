import type { PredicateWithIndex, RefinementWithIndex } from "fp-ts/FilterableWithIndex"
import type { Monoid } from "fp-ts/lib/Monoid"
import type { Predicate } from "fp-ts/Predicate"
import type { Refinement } from "fp-ts/Refinement"

import * as O from "fp-ts/Option"

export const make = <A>(iterator: () => Iterator<A>): Iterable<A> => ({
  [Symbol.iterator]() {
    return iterator()
  },
})

export const append =
  <B>(elem: B) =>
  <A>(iterable: Iterable<A>): Iterable<A | B> => {
    return make<A | B>(() => {
      let done = false
      const ia = iterable[Symbol.iterator]()
      let va: IteratorResult<A>
      return {
        next() {
          if (done) {
            return this.return!()
          }
          va = ia.next()
          if (va.done) {
            done = true
            return { done: false, value: elem }
          }
          return { done, value: va.value }
        },
        return(value?: unknown) {
          done = true
          if (!done) {
            done = true
          }
          if (typeof ia.return === "function") {
            ia.return()
          }
          return { done, value }
        },
      }
    })
  }

export const chainWithIndex =
  <A, B>(f: (i: number, a: A) => Iterable<B>) =>
  (iterable: Iterable<A>): Iterable<B> =>
    make<B>(() => {
      const ia = iterable[Symbol.iterator]()
      let ib: Iterator<B>
      let va: IteratorResult<A>
      let vb: IteratorResult<B>
      let done = false
      let i = 0

      const pullA = (onDone: () => IteratorResult<B>): IteratorResult<B> => {
        va = ia.next()
        if (va.done) {
          return onDone()
        }
        ib = f(i, va.value)[Symbol.iterator]()
        i++
        return pullB(onDone)
      }
      const pullB = (onDone: () => IteratorResult<B>): IteratorResult<B> => {
        if (!ib) {
          return pullA(onDone)
        }
        vb = ib!.next()
        if (!vb.done) {
          return { done, value: vb.value }
        }
        return pullA(onDone)
      }
      return {
        next() {
          if (done) {
            return this.return!()
          }
          return pullB(() => this.return!())
        },
        return(value?: unknown) {
          if (!done) {
            done = true
            if (typeof ia.return === "function") {
              ia.return()
            }
            if (ib && typeof ib.return === "function") {
              ib.return()
            }
          }
          return { done, value }
        },
      }
    })

export const chain = <A, B>(f: (a: A) => Iterable<B>): ((iterable: Iterable<A>) => Iterable<B>) =>
  chainWithIndex((_, a) => f(a))

export function corresponds_<A, B>(
  left: Iterable<A>,
  right: Iterable<B>,
  f: (a: A, b: B) => boolean,
): boolean {
  const leftIterator = left[Symbol.iterator]()
  const rightIterator = right[Symbol.iterator]()
  let lnext: IteratorResult<A>
  let rnext: IteratorResult<B>
  while (true) {
    lnext = leftIterator.next()
    rnext = rightIterator.next()
    if (lnext.done !== rnext.done) {
      return false
    }
    if (lnext.done === true) {
      return true
    }

    if (!f(lnext.value, rnext.value)) {
      return false
    }
  }
}

export const corresponds =
  <A, B>(right: Iterable<B>, f: (a: A, b: B) => boolean) =>
  (left: Iterable<A>): boolean =>
    corresponds_(left, right, f)

export function concat_<A, B>(left: Iterable<A>, right: Iterable<B>): Iterable<A | B> {
  return {
    [Symbol.iterator]() {
      const ia = left[Symbol.iterator]()
      let doneA = false
      let ib: Iterator<B>
      return {
        next() {
          if (!doneA) {
            const r = ia.next()
            if (r.done) {
              doneA = true
              ib = right[Symbol.iterator]()
              return ib.next()
            }
            return r
          }
          return ib.next()
        },
      }
    },
  }
}

export const concat =
  <B>(right: Iterable<B>) =>
  <A>(left: Iterable<A>): Iterable<A | B> =>
    concat_(left, right)

export function everyWithIndex<A, B extends A>(
  p: RefinementWithIndex<number, A, B>,
): (iterable: Iterable<A>) => iterable is Iterable<B>
export function everyWithIndex<A>(
  p: PredicateWithIndex<number, A>,
): (iterable: Iterable<A>) => boolean
export function everyWithIndex<A>(
  p: PredicateWithIndex<number, A>,
): (iterable: Iterable<A>) => boolean {
  return (iterable) => {
    const iterator = iterable[Symbol.iterator]()
    let i = 0
    let next: IteratorResult<A>
    while (!(next = iterator.next()).done) {
      if (!p(i, next.value)) {
        if (typeof iterator.return === "function") {
          iterator.return()
        }
        return false
      }
      i++
    }
    return true
  }
}

export function every<A, B extends A>(
  p: Refinement<A, B>,
): (iterable: Iterable<A>) => iterable is Iterable<B>
export function every<A>(p: Predicate<A>): (iterable: Iterable<A>) => boolean
export function every<A>(p: Predicate<A>): (iterable: Iterable<A>) => boolean {
  return everyWithIndex((_, a) => p(a))
}

export function filterWithIndex<A, B extends A>(
  p: RefinementWithIndex<number, A, B>,
): (iterable: Iterable<A>) => Iterable<B>
export function filterWithIndex<A>(
  p: PredicateWithIndex<number, A>,
): (iterable: Iterable<A>) => Iterable<A>
export function filterWithIndex<A>(
  p: PredicateWithIndex<number, A>,
): (iterable: Iterable<A>) => Iterable<A> {
  return (iterable) =>
    make<A>(() => {
      let done = false
      let i = 0
      const ia = iterable[Symbol.iterator]()
      let va: IteratorResult<A>
      return {
        next() {
          if (done) {
            return this.return!()
          }
          while (true) {
            va = ia.next()
            if (va.done) return this.return!()
            if (p(i++, va.value)) return { done, value: va.value }
          }
        },
        return(value?: unknown) {
          if (!done) {
            done = true
            if (typeof ia.return === "function") {
              ia.return()
            }
          }
          return { done, value }
        },
      }
    })
}

export function filter<A, B extends A>(p: Refinement<A, B>): (iterable: Iterable<A>) => Iterable<B>
export function filter<A>(p: Predicate<A>): (iterable: Iterable<A>) => Iterable<A>
export function filter<A>(p: Predicate<A>): (iterable: Iterable<A>) => Iterable<A> {
  return filterWithIndex((_, a) => p(a))
}

export const filterMapWithIndex =
  <A, B>(f: (i: number, a: A) => O.Option<B>) =>
  (iterable: Iterable<A>): Iterable<B> =>
    make<B>(() => {
      let i = 0
      const ia = iterable[Symbol.iterator]()
      let va: IteratorResult<A>
      let done = false
      return {
        next() {
          if (done) {
            this.return!()
          }
          // eslint-disable-next-line no-constant-condition
          while (true) {
            va = ia.next()
            if (va.done) {
              return this.return!()
            }
            const ob = f(i++, va.value)
            if (O.isSome(ob)) {
              return { done, value: ob.value }
            }
          }
        },
        return(value?: unknown) {
          if (!done) {
            done = true
            if (typeof ia.return === "function") {
              ia.return()
            }
          }
          return { done, value }
        },
      }
    })

export const filterMap = <A, B>(
  f: (a: A) => O.Option<B>,
): ((iterable: Iterable<A>) => Iterable<B>) => filterMapWithIndex((_, a) => f(a))

export const reduceWithIndex =
  <A, B>(b: B, f: (i: number, b: B, a: A) => B) =>
  (iterable: Iterable<A>): B => {
    let res = b
    let i = 0
    for (const value of iterable) {
      res = f(i++, res, value)
    }
    return res
  }

export const reduce = <A, B>(b: B, f: (b: B, a: A) => B): ((iterable: Iterable<A>) => B) =>
  reduceWithIndex(b, (_, b, a) => f(b, a))

export const foldMapWithIndex =
  <M>(M: Monoid<M>) =>
  <A>(f: (i: number, a: A) => M): ((fa: Iterable<A>) => M) =>
    reduceWithIndex(M.empty, (i, b, a) => M.concat(b, f(i, a)))

export const foldMap =
  <M>(M: Monoid<M>) =>
  <A>(f: (a: A) => M): ((fa: Iterable<A>) => M) =>
    foldMapWithIndex(M)((_, a) => f(a))

export const mapWithIndex =
  <A, B>(f: (i: number, a: A) => B) =>
  (iterable: Iterable<A>): Iterable<B> =>
    make<B>(() => {
      const ia = iterable[Symbol.iterator]()
      let i = 0
      let done = false
      let va: IteratorResult<A>
      return {
        next() {
          if (done) {
            return this.return!()
          }
          va = ia.next()
          if (va.done) {
            return this.return!()
          }
          return { done, value: f(i++, va.value) }
        },
        return(value?: unknown) {
          if (!done) {
            done = true
            if (typeof ia.return === "function") {
              ia.return()
            }
          }
          return { done, value }
        },
      }
    })

export const map = <A, B>(f: (a: A) => B): ((iterable: Iterable<A>) => Iterable<B>) =>
  mapWithIndex((_, a) => f(a))

export const take =
  (n: number) =>
  <A>(iterable: Iterable<A>): Iterable<A> =>
    make<A>(() => {
      let done = false
      let i = 0
      let value: IteratorResult<A>
      const iterator = iterable[Symbol.iterator]()
      return {
        next() {
          if (done || i >= n) {
            return this.return!()
          }
          value = iterator.next()
          i++
          if (value.done) {
            this.return!()
          }
          return value
        },
        return(value?: unknown) {
          if (!done) {
            done = true
            if (typeof iterator.return === "function") {
              iterator.return()
            }
          }
          return { done: true, value }
        },
      }
    })

export const zipWith =
  <A, B, C>(fb: Iterable<B>, f: (a: A, b: B) => C) =>
  (fa: Iterable<A>): Iterable<C> =>
    make<C>(() => {
      let done = false
      const ia = fa[Symbol.iterator]()
      const ib = fb[Symbol.iterator]()
      return {
        next() {
          if (done) {
            return this.return!()
          }

          const va = ia.next()
          const vb = ib.next()

          return va.done || vb.done ? this.return!() : { done: false, value: f(va.value, vb.value) }
        },
        return(value?: unknown) {
          if (!done) {
            done = true

            if (typeof ia.return === "function") {
              ia.return()
            }
            if (typeof ib.return === "function") {
              ib.return()
            }
          }

          return { done: true, value }
        },
      }
    })

export function zipWithIndex<A>(iterable: Iterable<A>): Iterable<readonly [number, A]> {
  return {
    [Symbol.iterator]() {
      let n = 0
      let done = false
      const iterator = iterable[Symbol.iterator]()
      return {
        next() {
          if (done) {
            this.return!()
          }
          const v = iterator.next()
          return v.done ? this.return!() : { done: false, value: [n++, v.value] }
        },
        return(value?: unknown) {
          if (!done) {
            done = true
            if (typeof iterator.return === "function") {
              iterator.return()
            }
          }
          return { done: true, value }
        },
      }
    },
  }
}

export function size<A>(iterable: Iterable<A>): number {
  if (
    "length" in iterable &&
    typeof (iterable as Iterable<A> & { length: number }).length === "number"
  ) {
    return (iterable as Iterable<A> & { length: number }).length
  }
  if ("size" in iterable && typeof (iterable as Iterable<A> & { size: number }).size === "number") {
    return (iterable as Iterable<A> & { size: number }).size
  }
  let i = 0
  const iterator = iterable[Symbol.iterator]()
  let _result: IteratorResult<A>
  while (!(_result = iterator.next()).done) {
    i++
  }
  return i
}
