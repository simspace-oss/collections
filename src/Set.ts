import type { HKT, Kind, URIS } from "fp-ts/HKT"
import type { Zero, Zero1 } from "fp-ts/Zero"

/**
 * @category type classes
 * @since 0.0.0
 */
export interface Set<S> extends Zero<S> {
  readonly size: <V>(set: HKT<S, V>) => number
  readonly add: <V>(value: V) => (set: HKT<S, V>) => HKT<S, V>
  readonly has: <V>(value: V) => (set: HKT<S, V>) => boolean
  readonly remove: <V>(value: V) => (set: HKT<S, V>) => HKT<S, V>
  readonly union: <V>(sb: HKT<S, V>) => (sa: HKT<S, V>) => HKT<S, V>
  readonly intersection: <V>(sb: HKT<S, V>) => (sa: HKT<S, V>) => HKT<S, V>
  readonly difference: <V>(sb: HKT<S, V>) => (sa: HKT<S, V>) => HKT<S, V>
  readonly isSubset: <V>(sb: HKT<S, V>) => (sa: HKT<S, V>) => boolean
  readonly fromReadonlyArray: <V>(ra: ReadonlyArray<V>) => HKT<S, V>
}

/**
 * @category type classes
 * @since 0.0.0
 */
export interface Set1<S extends URIS> extends Zero1<S> {
  readonly size: <V>(set: Kind<S, V>) => number
  readonly add: <V>(value: V) => (set: Kind<S, V>) => Kind<S, V>
  readonly has: <V>(value: V) => (set: Kind<S, V>) => boolean
  readonly remove: <V>(value: V) => (set: Kind<S, V>) => Kind<S, V>
  readonly union: <V>(sb: Kind<S, V>) => (sa: Kind<S, V>) => Kind<S, V>
  readonly intersection: <V>(sb: Kind<S, V>) => (sa: Kind<S, V>) => Kind<S, V>
  readonly difference: <V>(sb: Kind<S, V>) => (sa: Kind<S, V>) => Kind<S, V>
  readonly isSubset: <V>(sb: Kind<S, V>) => (sa: Kind<S, V>) => boolean
  readonly fromReadonlyArray: <V>(ra: ReadonlyArray<V>) => Kind<S, V>
}

export interface Set1C<S extends URIS, V> {
  readonly URI: S
  readonly size: (set: Kind<S, V>) => number
  readonly add: (value: V) => (set: Kind<S, V>) => Kind<S, V>
  readonly has: (value: V) => (set: Kind<S, V>) => boolean
  readonly remove: (value: V) => (set: Kind<S, V>) => Kind<S, V>
  readonly union: (sb: Kind<S, V>) => (sa: Kind<S, V>) => Kind<S, V>
  readonly intersection: (sb: Kind<S, V>) => (sa: Kind<S, V>) => Kind<S, V>
  readonly difference: (sb: Kind<S, V>) => (sa: Kind<S, V>) => Kind<S, V>
  readonly isSubset: <V>(sb: Kind<S, V>) => (sa: Kind<S, V>) => boolean
  readonly fromReadonlyArray: <V>(ra: ReadonlyArray<V>) => Kind<S, V>
  // there's no Zero1C or Foldable1C
  readonly zero: () => Kind<S, V>
}
