import type { Hash } from "./Hashable"
import type { Eq } from "fp-ts/Eq"

import { deepEqualsUnknown, shallowEqualsUnknown, strictEqualsUnknown } from "./Equatable"
import { unknown } from "./Hashable"

export interface HashEq<A> extends Hash<A>, Eq<A> {}

export const getDefaultStrict = <A>(): HashEq<A> => ({
  hash: unknown,
  equals: strictEqualsUnknown,
})

export const getDefaultShallow = <A>(): HashEq<A> => ({
  hash: unknown,
  equals: shallowEqualsUnknown,
})

export const getDefaultDeep = <A>(): HashEq<A> => ({
  hash: unknown,
  equals: deepEqualsUnknown,
})
