export function isObject(value: unknown): value is Record<string, unknown> {
  return typeof value === "object" && value !== null
}

export function isPlainObject(value: unknown): value is Record<string, unknown> {
  return isObject(value) && (value.constructor === Object || value.constructor === null)
}

export function isIterable(value: unknown): value is Iterable<unknown> {
  return isObject(value) && Symbol.iterator in value
}

export function hasTypeId<A, B extends symbol>(a: A, typeId: B): a is A & { _typeId: B } {
  return isObject(a) && "_typeId" in a && a._typeId === typeId
}
