/* eslint-disable no-bitwise */
import * as O from "fp-ts/Option"

import { arraySpliceIn, arraySpliceOut, arrayUpdate } from "./array"
import { fromBitmap, hashFragment, toBitmap } from "./bitwise"
import { MAX_INDEX_NODE, MIN_ARRAY_NODE, SIZE } from "./constants"

export type KeyEq<K> = (x: K, y: K) => boolean

export type UpdateFn<V> = (v: O.Option<V>) => O.Option<V>

export interface SizeRef {
  value: number
}

export type Node<K, V> =
  | EmptyNode<K, V>
  | LeafNode<K, V>
  | CollisionNode<K, V>
  | ArrayNode<K, V>
  | IndexedNode<K, V>

export function isEmptyNode<K, V>(node: Node<K, V> | undefined): node is EmptyNode<K, V> {
  return node === _EmptyNode || (!!node && node._tag === "EmptyNode")
}

function canEditNode<K, V>(edit: number, node: Node<K, V>): boolean {
  return !isEmptyNode(node) && edit === node.edit
}

function isLeaf<K, V>(node: Node<K, V>): boolean {
  return node._tag === "LeafNode" || node._tag === "CollisionNode" || node._tag === "EmptyNode"
}

/**
 * Empty Node
 */
export class EmptyNode<K, V> {
  readonly _tag = "EmptyNode"

  modify(
    edit: number,
    _keyEq: KeyEq<K>,
    _shift: number,
    f: (v: O.Option<V>) => O.Option<V>,
    hash: number,
    key: K,
    size: SizeRef,
  ) {
    const v = f(O.none)
    if (O.isNone(v)) {
      return _EmptyNode
    }
    ++size.value
    return new LeafNode(edit, hash, key, v)
  }
}

const _EmptyNode = new EmptyNode<never, never>()

/**
 * Leaf holding a value
 */
export class LeafNode<K, V> {
  readonly _tag = "LeafNode"
  constructor(
    readonly edit: number,
    readonly hash: number,
    readonly key: K,
    public value: O.Option<V>,
  ) {}

  modify(
    edit: number,
    keyEq: KeyEq<K>,
    shift: number,
    f: UpdateFn<V>,
    h: number,
    k: K,
    size: SizeRef,
  ): Node<K, V> {
    if (keyEq(k, this.key)) {
      const v = f(this.value)
      if (
        v === this.value ||
        (O.isSome(v) && O.isSome(this.value) && v.value === this.value.value)
      ) {
        return this
      } else if (O.isNone(v)) {
        --size.value
        return _EmptyNode as Node<K, V>
      }
      if (canEditNode(edit, this)) {
        this.value = v
        return this
      }
      return new LeafNode(edit, h, k, v)
    }
    const v = f(O.none)
    if (O.isNone(v)) {
      return this
    }
    ++size.value
    return mergeLeaves(edit, shift, this.hash, this, h, new LeafNode(edit, h, k, v))
  }
}

/**
 * Leaf holding multiple values with the same hash, but different keys
 */
export class CollisionNode<K, V> {
  readonly _tag = "CollisionNode"
  constructor(readonly edit: number, readonly hash: number, readonly children: Array<Node<K, V>>) {}

  modify(
    edit: number,
    keyEq: KeyEq<K>,
    shift: number,
    f: UpdateFn<V>,
    h: number,
    k: K,
    size: SizeRef,
  ): Node<K, V> {
    if (h === this.hash) {
      const canEdit = canEditNode(edit, this)
      const list = updateCollisionList(canEdit, edit, keyEq, this.hash, this.children, f, k, size)
      if (list === this.children) {
        return this
      }

      return list.length > 1 ? new CollisionNode(edit, this.hash, list) : list[0] // collapse single element collision list
    }
    const v = f(O.none)
    if (O.isNone(v)) {
      return this
    }
    ++size.value
    return mergeLeaves(edit, shift, this.hash, this, h, new LeafNode(edit, h, k, v))
  }
}

/**
 * Internal node with many children
 */
export class ArrayNode<K, V> {
  readonly _tag = "ArrayNode"
  constructor(readonly edit: number, public size: number, public children: Array<Node<K, V>>) {}

  modify(
    edit: number,
    keyEq: KeyEq<K>,
    shift: number,
    f: UpdateFn<V>,
    h: number,
    k: K,
    size: SizeRef,
  ): Node<K, V> {
    let count = this.size
    const children = this.children
    const frag = hashFragment(shift, h)
    const child = children[frag]
    const newChild = (child ?? _EmptyNode).modify(edit, keyEq, shift + SIZE, f, h, k, size)

    if (child === newChild) {
      return this
    }

    const canEdit = canEditNode(edit, this)
    let newChildren
    if (isEmptyNode(child) && !isEmptyNode(newChild)) {
      // add
      ++count
      newChildren = arrayUpdate(canEdit, frag, newChild, children)
    } else if (!isEmptyNode(child) && isEmptyNode(newChild)) {
      // remove
      --count
      if (count <= MIN_ARRAY_NODE) {
        return pack(edit, count, frag, children)
      }
      newChildren = arrayUpdate(canEdit, frag, _EmptyNode, children)
    } else {
      // modify
      newChildren = arrayUpdate(canEdit, frag, newChild, children)
    }

    if (canEdit) {
      this.size = count
      this.children = newChildren
      return this
    }
    return new ArrayNode(edit, count, newChildren)
  }
}

/**
 * Internal node with a sparse set of children.
 *
 * Uses a bitmap and array to pack children.
 */
export class IndexedNode<K, V> {
  readonly _tag = "IndexedNode"
  constructor(readonly edit: number, public mask: number, public children: Array<Node<K, V>>) {}

  modify(
    edit: number,
    keyEq: KeyEq<K>,
    shift: number,
    f: UpdateFn<V>,
    h: number,
    k: K,
    size: SizeRef,
  ): Node<K, V> {
    const mask = this.mask
    const children = this.children
    const frag = hashFragment(shift, h)
    const bit = toBitmap(frag)
    const indx = fromBitmap(mask, bit)
    const exists = mask & bit
    const current = exists ? children[indx] : _EmptyNode
    const child = current.modify(edit, keyEq, shift + SIZE, f, h, k, size)

    if (current === child) {
      return this
    }

    const canEdit = canEditNode(edit, this)
    let bitmap = mask
    let newChildren: Array<Node<K, V>>
    if (exists && isEmptyNode(child)) {
      // remove
      bitmap &= ~bit
      if (!bitmap) {
        return _EmptyNode
      }
      if (children.length <= 2 && isLeaf(children[indx ^ 1])) {
        return children[indx ^ 1]
      } // collapse

      newChildren = arraySpliceOut(canEdit, indx, children)
    } else if (!exists && !isEmptyNode(child)) {
      // add
      if (children.length >= MAX_INDEX_NODE) {
        return expand(edit, frag, child, mask, children)
      }

      bitmap |= bit
      newChildren = arraySpliceIn(canEdit, indx, child, children)
    } else {
      // modify
      newChildren = arrayUpdate(canEdit, indx, child, children)
    }

    if (canEdit) {
      this.mask = bitmap
      this.children = newChildren
      return this
    }
    return new IndexedNode(edit, bitmap, newChildren)
  }
}

/**
 * Expand an IndexedNode into an ArrayNode
 */
function expand<K, V>(
  edit: number,
  frag: number,
  child: Node<K, V>,
  bitmap: number,
  subNodes: Array<Node<K, V>>,
): ArrayNode<K, V> {
  const arr = []
  let bit = bitmap
  let count = 0
  for (let i = 0; bit; ++i) {
    if (bit & 1) {
      arr[i] = subNodes[count++]
    }
    bit >>>= 1
  }
  arr[frag] = child
  return new ArrayNode(edit, count + 1, arr)
}

/**
 * Collapse an ArrayNode into an IndexedNode
 */
function pack<K, V>(
  edit: number,
  count: number,
  removed: number,
  elements: Array<Node<K, V>>,
): IndexedNode<K, V> {
  const children = new Array<Node<K, V>>(count - 1)
  let g = 0
  let bitmap = 0
  for (let i = 0, len = elements.length; i < len; ++i) {
    if (i !== removed) {
      const elem = elements[i]
      if (elem !== undefined && !isEmptyNode(elem)) {
        children[g++] = elem
        bitmap |= 1 << i
      }
    }
  }
  return new IndexedNode(edit, bitmap, children)
}

function mergeLeaves<K, V>(
  edit: number,
  shift: number,
  h1: number,
  n1: Node<K, V>,
  h2: number,
  n2: Node<K, V>,
): Node<K, V> {
  if (h1 === h2) {
    return new CollisionNode(edit, h1, [n2, n1])
  }

  const subH1 = hashFragment(shift, h1)
  const subH2 = hashFragment(shift, h2)
  return new IndexedNode(
    edit,
    toBitmap(subH1) | toBitmap(subH2),
    subH1 === subH2
      ? [mergeLeaves(edit, shift + SIZE, h1, n1, h2, n2)]
      : subH1 < subH2
      ? [n1, n2]
      : [n2, n1],
  )
}

function updateCollisionList<K, V>(
  mutate: boolean,
  edit: number,
  keyEq: KeyEq<K>,
  h: number,
  list: Array<Node<K, V>>,
  f: UpdateFn<V>,
  k: K,
  size: SizeRef,
): Array<Node<K, V>> {
  const len = list.length
  for (let i = 0; i < len; ++i) {
    const child = list[i]
    if ("key" in child && keyEq(k, child.key)) {
      const value = child.value
      const newValue = f(value)
      if (
        newValue === value ||
        (O.isSome(value) && O.isSome(newValue) && value.value === newValue.value)
      ) {
        return list
      }

      if (O.isNone(newValue)) {
        --size.value
        return arraySpliceOut(mutate, i, list)
      }
      return arrayUpdate(mutate, i, new LeafNode(edit, h, k, newValue), list)
    }
  }

  const newValue = f(O.none)
  if (O.isNone(newValue)) {
    return list
  }
  ++size.value
  return arrayUpdate(mutate, len, new LeafNode(edit, h, k, newValue), list)
}
