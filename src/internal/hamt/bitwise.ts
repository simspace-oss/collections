/* eslint-disable no-bitwise */

import { MASK } from "./constants"

export function popcount(x: number): number {
  x -= (x >> 1) & 0x55555555
  x = (x & 0x33333333) + ((x >> 2) & 0x33333333)
  x = (x + (x >> 4)) & 0x0f0f0f0f
  x += x >> 8
  x += x >> 16
  return x & 0x7f
}

export function hashFragment(shift: number, h: number): number {
  return (h >>> shift) & MASK
}

export function toBitmap(x: number): number {
  return 1 << x
}

export function fromBitmap(bitmap: number, bit: number): number {
  return popcount(bitmap & (bit - 1))
}
