import type { Node } from "./node"

import * as O from "fp-ts/Option"

import { isEmptyNode } from "./node"

/**
 * @internal
 */
export type TraversalFn<K, V, A> = (node: [K, V]) => A

/**
 * @internal
 */
export type KeyTraversalFn<K, A> = (key: K) => A

/**
 * @internal
 */
export type Cont<K, V, A> =
  | [
      len: number,
      children: Array<Node<K, V>>,
      i: number,
      f: TraversalFn<K, V, A>,
      cont: Cont<K, V, A>,
    ]
  | undefined

/**
 * @internal
 */
export interface VisitResult<K, V, A> {
  value: A
  cont: Cont<K, V, A>
}

/**
 * @internal
 */
export function applyCont<K, V, A>(cont: Cont<K, V, A>) {
  return cont ? visitLazyChildren(cont[0], cont[1], cont[2], cont[3], cont[4]) : O.none
}

/**
 * @internal
 */
export function visitLazy<K, V, A>(
  node: Node<K, V>,
  f: TraversalFn<K, V, A>,
  cont: Cont<K, V, A> = undefined,
): O.Option<VisitResult<K, V, A>> {
  switch (node._tag) {
    case "LeafNode": {
      if (O.isSome(node.value)) {
        return O.some({
          value: f([node.key, node.value.value]),
          cont,
        })
      } else {
        return applyCont(cont)
      }
    }
    case "CollisionNode":
    case "ArrayNode":
    case "IndexedNode": {
      const children = node.children
      return visitLazyChildren(children.length, children, 0, f, cont)
    }
    default: {
      return applyCont(cont)
    }
  }
}

/**
 * @internal
 */
export function visitLazyChildren<K, V, A>(
  len: number,
  children: Array<Node<K, V>>,
  i: number,
  f: TraversalFn<K, V, A>,
  cont: Cont<K, V, A>,
): O.Option<VisitResult<K, V, A>> {
  while (i < len) {
    const child = children[i++]
    if (child !== undefined && !isEmptyNode(child)) {
      return visitLazy(child, f, [len, children, i, f, cont])
    }
  }
  return applyCont(cont)
}
