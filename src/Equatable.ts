import { deepEqual, sameValueZeroEqual, shallowEqual } from "fast-equals"
import { fromEquals } from "fp-ts/Eq"

import { isObject } from "./internal/guards"

export const equatableSymbol = Symbol.for("@simspace/collections/Equatable")

export interface Equatable {
  [equatableSymbol](that: unknown): boolean
}

export const isEquatable = (u: unknown): u is Equatable => isObject(u) && equatableSymbol in u

export const strictEqualsUnknown = (a: unknown, b: unknown): boolean => {
  if (isEquatable(a)) {
    return a[equatableSymbol](b)
  }
  if (isEquatable(b)) {
    return b[equatableSymbol](a)
  }
  return sameValueZeroEqual(a, b)
}

export const shallowEqualsUnknown = (a: unknown, b: unknown): boolean => {
  if (isEquatable(a)) {
    return a[equatableSymbol](b)
  }
  if (isEquatable(b)) {
    return b[equatableSymbol](a)
  }
  return shallowEqual(a, b)
}

export const deepEqualsUnknown = (a: unknown, b: unknown): boolean => {
  if (isEquatable(a)) {
    return a[equatableSymbol](b)
  }
  if (isEquatable(b)) {
    return b[equatableSymbol](a)
  }
  return deepEqual(a, b)
}

export const EqStrict = fromEquals(strictEqualsUnknown)

export const EqShallow = fromEquals(shallowEqualsUnknown)

export const EqDeep = fromEquals(deepEqualsUnknown)
