import type { HKT2, Kind2, URIS2 } from "fp-ts/HKT"
import type { Option } from "fp-ts/Option"
import type { Semigroup } from "fp-ts/Semigroup"

/**
 * @category type classes
 */
export interface Map<M> {
  readonly URI: M
  readonly set: <K, V>(key: K, value: V) => (map: HKT2<M, K, V>) => HKT2<M, K, V>
  readonly size: <K, V>(map: HKT2<M, K, V>) => number
  readonly has: <K>(key: K) => <V>(map: HKT2<M, K, V>) => boolean
  readonly remove: <K>(key: K) => <V>(map: HKT2<M, K, V>) => HKT2<M, K, V>
  readonly get: <K>(key: K) => <V>(map: HKT2<M, K, V>) => Option<V>
  readonly zero: <K, V>() => HKT2<M, K, V>

  readonly union: <V>(
    S: Semigroup<V>,
  ) => <K>(mkb: HKT2<M, K, V>) => (mka: HKT2<M, K, V>) => HKT2<M, K, V>

  readonly intersection: <V>(
    S: Semigroup<V>,
  ) => <K>(mkb: HKT2<M, K, V>) => (mka: HKT2<M, K, V>) => HKT2<M, K, V>

  readonly fromReadonlyArray: <K, V>(ra: ReadonlyArray<readonly [K, V]>) => HKT2<M, K, V>
}

/**
 * @category type classes
 */
export interface Map2<M extends URIS2> {
  readonly URI: M
  readonly size: <K, V>(map: Kind2<M, K, V>) => number
  readonly set: <K, V>(key: K, value: V) => (map: Kind2<M, K, V>) => Kind2<M, K, V>
  readonly has: <K>(key: K) => <V>(map: Kind2<M, K, V>) => boolean
  readonly remove: <K>(key: K) => <V>(map: Kind2<M, K, V>) => Kind2<M, K, V>
  readonly get: <K>(key: K) => <V>(map: Kind2<M, K, V>) => Option<V>
  readonly zero: <K, V>() => Kind2<M, K, V>

  readonly union: <V>(
    S: Semigroup<V>,
  ) => <K>(mkb: Kind2<M, K, V>) => (mka: Kind2<M, K, V>) => Kind2<M, K, V>

  readonly intersection: <V>(
    S: Semigroup<V>,
  ) => <K>(mkb: Kind2<M, K, V>) => (mka: Kind2<M, K, V>) => Kind2<M, K, V>

  readonly fromReadonlyArray: <K, V>(ra: ReadonlyArray<readonly [K, V]>) => Kind2<M, K, V>
}

/**
 * @category type classes
 */
export interface Map2C<M extends URIS2, K> {
  readonly URI: M
  readonly size: <V>(map: Kind2<M, K, V>) => number
  readonly set: <K, V>(key: K, value: V) => (mkv: Kind2<M, K, V>) => Kind2<M, K, V>
  readonly has: (key: K) => <V>(mkv: Kind2<M, K, V>) => boolean
  readonly remove: (key: K) => <V>(mkv: Kind2<M, K, V>) => Kind2<M, K, V>
  readonly get: (key: K) => <V>(mkv: Kind2<M, K, V>) => Option<V>
  readonly zero: <V>() => Kind2<M, K, V>

  readonly union: <V>(
    S: Semigroup<V>,
  ) => <K>(mkb: Kind2<M, K, V>) => (mka: Kind2<M, K, V>) => Kind2<M, K, V>

  readonly intersection: <V>(
    S: Semigroup<V>,
  ) => <K>(mkb: Kind2<M, K, V>) => (mka: Kind2<M, K, V>) => Kind2<M, K, V>

  readonly fromReadonlyArray: <V>(ra: ReadonlyArray<readonly [K, V]>) => Kind2<M, K, V>
}
