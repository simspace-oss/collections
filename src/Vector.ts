import type { Predicate } from "fp-ts/Predicate"
import type { ReadonlyNonEmptyArray } from "fp-ts/ReadonlyNonEmptyArray"
import type { Refinement } from "fp-ts/Refinement"
import type { Separated } from "fp-ts/Separated"

import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import { not } from "fp-ts/Predicate"
import * as L from "list"

/**
 * `Vector` is a fast and immutable sequential data-structure, suitable for
 * use wherever one might use an immutable Array.
 */
export type Vector<A> = L.List<A>

/**
 * Creates an empty Vector.
 */
export const empty: <A>() => Vector<A> = L.empty

/**
 * Takes a single arguments and returns a singleton vector that contains it.
 */
export const of: <A>(a: A) => Vector<A> = L.of

/**
 * Converts an array, an array-like, or an iterable into a vector.
 */
export const from: <A>(sequence: ReadonlyArray<A> | Iterable<A> | ArrayLike<A>) => Vector<A> =
  L.from

export const vector: <A>(...elements: ReadonlyArray<A>) => Vector<A> = L.list

export const range: (start: number, end: number) => Vector<number> = L.range

export const replicate =
  (n: number) =>
  <A>(value: A): Vector<A> =>
    L.repeat(value, n)

/**
 * Appends an element to the end of a Vector and returns the new Vector.
 */
export const append =
  <A>(value: A) =>
  (v: Vector<A>): Vector<A> =>
    L.append(value, v)

/**
 * Prepends an element to the front of a vector and returns the new vector.
 */
export const prepend =
  <A>(value: A) =>
  (v: Vector<A>): Vector<A> =>
    L.prepend(value, v)

/**
 * Concatenates two vectors.
 */
export const concat =
  <A>(second: Vector<A>) =>
  (first: Vector<A>): Vector<A> =>
    L.concat(first, second)

/**
 * Applies a function to each element in the given vector and returns a
 * new vector of the values that the function return.
 */
export const map =
  <A, B>(f: (a: A) => B) =>
  (v: Vector<A>): Vector<B> =>
    L.map(f, v)

/**
 * Maps a function over a vector and concatenates all the resulting
 * vectors together.
 */
export const chain =
  <A, B>(f: (a: A) => Vector<B>) =>
  (v: Vector<A>): Vector<B> =>
    L.flatMap(f, v)

/**
 * Flattens a vector of vectors into a vector.
 */
export const flatten: <A>(v: Vector<Vector<A>>) => Vector<A> = L.flatten

/**
 * Folds a function over a vector. Left-associative.
 */
export const reduce =
  <A, B>(b: B, f: (b: B, a: A) => B) =>
  (v: Vector<A>): B =>
    L.reduce(f, b, v)

/**
 * Folds a function over a vector. Right-associative.
 */
export const reduceRight =
  <A, B>(b: B, f: (a: A, b: B) => B) =>
  (v: Vector<A>): B =>
    L.reduceRight(f, b, v)

/**
 * Returns a new vector that only contains the elements of the original
 * list for which the predicate returns `true`.
 */
export function filter<A, B extends A>(p: Refinement<A, B>): (v: Vector<A>) => Vector<B>
export function filter<A>(p: Predicate<A>): (v: Vector<A>) => Vector<A>
export function filter<A>(p: Predicate<A>) {
  return (v: Vector<A>): Vector<A> => L.filter(p, v)
}

export const filterMap = <A, B>(f: (a: A) => O.Option<B>): ((v: Vector<A>) => Vector<B>) =>
  reduce(empty(), (v, a) =>
    pipe(
      f(a),
      O.match(
        () => v,
        (b) => append(b)(v),
      ),
    ),
  )

/**
 * Splits the vector into two vectors. One vector that contains all the
 * values for which the predicate returns `true` and one containing
 * the values for which it returns `false`.
 */
export function partition<A, B extends A>(
  p: Refinement<A, B>,
): (v: Vector<A>) => Separated<Vector<A>, Vector<B>>
export function partition<A>(p: Predicate<A>): (v: Vector<A>) => Separated<Vector<A>, Vector<A>>
export function partition<A>(p: Predicate<A>) {
  return (v: Vector<A>): Separated<Vector<A>, Vector<A>> => {
    const partitioned = L.partition(p, v)
    return { left: partitioned[0], right: partitioned[1] }
  }
}

export const partitionMap = <A, B, C>(
  f: (a: A) => E.Either<B, C>,
): ((v: Vector<A>) => Separated<Vector<B>, Vector<C>>) =>
  reduce({ left: empty(), right: empty() }, (acc, a) =>
    pipe(
      f(a),
      E.match(
        (b) => ({
          left: append(b)(acc.left),
          right: acc.right,
        }),
        (c) => ({
          left: acc.left,
          right: append(c)(acc.right),
        }),
      ),
    ),
  )

/**
 * Returns a slice of a vector. Elements are removed from the beginning and
 * end. Both the indices can be negative in which case they will count
 * from the right end of the vector.
 */
export const slice =
  (from: number, to: number) =>
  <A>(v: Vector<A>): Vector<A> =>
    L.slice(from, to, v)

/**
 * Returns the first element of the vector. If the vector is empty the
 * function returns None.
 */
export const head: <A>(v: Vector<A>) => O.Option<A> = O.fromNullableK(L.head)

/**
 * Returns a new vector with the first element removed. If the vector is
 * empty the empty vector is returned.
 */
export const tail: <A>(v: Vector<A>) => Vector<A> = L.tail

/**
 * Returns a new vector without the first `n` elements.
 */
export const drop =
  (n: number) =>
  <A>(v: Vector<A>): Vector<A> =>
    L.drop(n, v)

export const take =
  (n: number) =>
  <A>(v: Vector<A>): Vector<A> =>
    L.take(n, v)

export function every<A, B extends A>(p: Refinement<A, B>): (v: Vector<A>) => v is Vector<B>
export function every<A>(p: Predicate<A>): (v: Vector<A>) => boolean
export function every<A>(p: Predicate<A>): (v: Vector<A>) => boolean {
  return (v) => L.every(p, v)
}

export const isEmpty = L.isEmpty

export const isNonEmpty: <A>(v: Vector<A>) => boolean = not(isEmpty)

export const toArray = L.toArray

export const toReadonlyArray: <A>(v: Vector<A>) => ReadonlyArray<A> = L.toArray

export const toReadonlyNonEmptyArray = <A>(v: Vector<A>): O.Option<ReadonlyNonEmptyArray<A>> =>
  isNonEmpty(v) ? O.some(toReadonlyArray(v) as unknown as ReadonlyNonEmptyArray<A>) : O.none
