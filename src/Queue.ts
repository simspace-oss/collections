import type { Predicate } from "fp-ts/Predicate"

import * as O from "fp-ts/Option"

import * as It from "./Iterable"
import * as L from "./List"

export class Queue<A> implements Iterable<A> {
  constructor(
    /* @internal */
    public _in: L.List<A>,
    /* @internal */
    public _out: L.List<A>,
  ) {}

  [Symbol.iterator]() {
    return It.concat_(this._in, L.reverse(this._out))[Symbol.iterator]()
  }
}

const emptyQueue: Queue<never> = new Queue(L._Nil, L._Nil)

export function empty<A>(): Queue<A> {
  return emptyQueue
}

export function single<A>(a: A): Queue<A> {
  return new Queue(L._Nil, L.cons(a))
}

export function isEmpty<A>(queue: Queue<A>): boolean {
  return L.isEmpty(queue._in) && L.isEmpty(queue._out)
}

export function length<A>(queue: Queue<A>): number {
  return L.length(queue._in) + L.length(queue._out)
}

export function unsafeHead<A>(queue: Queue<A>): A | undefined {
  if (L.isNonEmpty(queue._out)) {
    return L.unsafeHead(queue._out)
  } else if (L.isNonEmpty(queue._in)) {
    return L.unsafeLast(queue._in)
  } else {
    return undefined
  }
}

export function head<A>(queue: Queue<A>): O.Option<A> {
  return O.fromNullable(unsafeHead(queue))
}

export function unsafeTail<A>(queue: Queue<A>): Queue<A> | undefined {
  if (L.isNonEmpty(queue._out)) {
    return new Queue(queue._in, queue._out.tail)
  } else if (L.isNonEmpty(queue._in)) {
    return new Queue(L.nil(), L.unsafeTail(L.reverse(queue._in))!)
  } else {
    return undefined
  }
}

export function tail<A>(queue: Queue<A>): O.Option<Queue<A>> {
  return O.fromNullable(unsafeTail(queue))
}

export const prepend =
  <B>(elem: B) =>
  <A>(queue: Queue<A>): Queue<A | B> =>
    new Queue(queue._in, L.prepend(elem)(queue._out))

export const enqueue =
  <B>(elem: B) =>
  <A>(queue: Queue<A>): Queue<A | B> =>
    new Queue(L.prepend(elem)(queue._in), queue._out)

export function unasfeDequeue<A>(queue: Queue<A>): readonly [A, Queue<A>] | undefined {
  if (L.isEmpty(queue._out) && L.isNonEmpty(queue._in)) {
    const rev = L.reverse(queue._in)
    return [L.unsafeHead(rev)!, new Queue(L.nil(), L.unsafeTail(rev)!)]
  } else if (L.isNonEmpty(queue._out)) {
    return [L.unsafeHead(queue._out)!, new Queue(queue._in, L.unsafeTail(queue._out)!)]
  } else {
    return undefined
  }
}

export function dequeue<A>(queue: Queue<A>): O.Option<readonly [A, Queue<A>]> {
  return O.fromNullable(unasfeDequeue(queue))
}

export const map =
  <A, B>(f: (a: A) => B) =>
  (fa: Queue<A>): Queue<B> =>
    new Queue(L.map(f)(fa._in), L.map(f)(fa._out))

export const reduce =
  <A, B>(b: B, f: (b: B, a: A) => B) =>
  (fa: Queue<A>): B => {
    let acc = b
    let these = fa
    while (!isEmpty(these)) {
      acc = f(acc, unsafeHead(these)!)
      these = unsafeTail(these)!
    }
    return acc
  }

export const exists =
  <A>(p: Predicate<A>) =>
  (queue: Queue<A>): boolean =>
    L.exists(p)(queue._in) || L.exists(p)(queue._out)

export const find =
  <A>(p: Predicate<A>) =>
  (queue: Queue<A>): O.Option<A> => {
    let these = queue
    while (!isEmpty(these)) {
      const head = unsafeHead(these)!
      if (p(head)) {
        return O.some(head)
      }
      these = unsafeTail(these)!
    }
    return O.none
  }

export const filter =
  <A>(p: Predicate<A>) =>
  (queue: Queue<A>): Queue<A> =>
    new Queue(L.filter(p)(queue._in), L.filter(p)(queue._out))

export const count = <A>(p: Predicate<A>): ((queue: Queue<A>) => number) =>
  reduce(0, (b, a) => (p(a) ? b + 1 : b))
