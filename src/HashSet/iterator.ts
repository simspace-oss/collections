import type { KeyTraversalFn, VisitResult } from "../internal/hamt/iterator"
import type { HashSetInternal } from "./definition"

import * as O from "fp-ts/Option"

import { applyCont, visitLazy } from "../internal/hamt/iterator"

export class HashSetIterator<A, B> implements IterableIterator<B> {
  v: O.Option<VisitResult<A, void, B>>
  constructor(readonly hashSet: HashSetInternal<A>, readonly f: KeyTraversalFn<A, B>) {
    this.v = visitLazy(this.hashSet.root, (node) => this.f(node[0]), undefined)
  }

  next(): IteratorResult<B> {
    if (O.isNone(this.v)) {
      return { done: true, value: undefined }
    }
    const v0 = this.v.value
    this.v = applyCont(v0.cont)
    return { done: false, value: v0.value }
  }

  [Symbol.iterator](): IterableIterator<B> {
    return new HashSetIterator(this.hashSet, this.f)
  }
}
