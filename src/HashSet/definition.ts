import type { HashEq } from "../HashEq"
import type { Node } from "../internal/hamt/node"

import { identity } from "fp-ts/function"

import * as Equ from "../Equatable"
import * as H from "../Hashable"
import { hasTypeId } from "../internal/guards"
import { corresponds_ } from "../Iterable"
import { HashSetIterator } from "./iterator"

export const HashSetTypeId = Symbol.for("@simspace/collection/HashSet")
export type HashSetTypeId = typeof HashSetTypeId

export interface HashSet<A> extends Iterable<A>, H.Hashable, Equ.Equatable {
  readonly _typeId: HashSetTypeId
  readonly _A: () => A
}

/**
 * @internal
 */
export class HashSetInternal<A> implements HashSet<A> {
  readonly _typeId: typeof HashSetTypeId = HashSetTypeId
  readonly _A!: () => A

  constructor(
    public editable: boolean,
    public edit: number,
    public root: Node<A, void>,
    public size: number,
    readonly config: HashEq<A>,
  ) {}

  [Symbol.iterator](): Iterator<A> {
    return new HashSetIterator(this, identity)
  }

  [H.hashableSymbol](): number {
    return H.combine(H.symbol(this._typeId), H.iterable(this))
  }

  [Equ.equatableSymbol](that: unknown): boolean {
    if (!isHashSet(that) || this.size !== that.size) {
      return false
    }
    return corresponds_(this, that, Equ.strictEqualsUnknown)
  }
}

function isHashSet(u: unknown): u is HashSetInternal<unknown> {
  return hasTypeId(u, HashSetTypeId)
}

export function concrete<A>(_: HashSet<A>): asserts _ is HashSetInternal<A> {
  //
}
