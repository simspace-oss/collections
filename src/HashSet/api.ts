/* eslint-disable no-bitwise */
import type { HashEq } from "../HashEq"
import type { Node, UpdateFn } from "../internal/hamt/node"
import type { HashSet } from "./definition"
import type { Ord } from "fp-ts/lib/Ord"

import { flow, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { getDefaultDeep } from "../HashEq"
import { fromBitmap, hashFragment, toBitmap } from "../internal/hamt/bitwise"
import { SIZE } from "../internal/hamt/constants"
import { EmptyNode, isEmptyNode } from "../internal/hamt/node"
import { concrete, HashSetInternal } from "./definition"

export const has =
  <A>(value: A) =>
  (set: HashSet<A>): boolean => {
    concrete(set)
    return O.isSome(getHash(set, value, set.config.hash(value)))
  }

export const add =
  <A>(value: A) =>
  (set: HashSet<A>): HashSet<A> => {
    concrete(set)
    return modifyHash(set, value, set.config.hash(value), () => O.some(void 0))
  }

export const remove =
  <A>(value: A) =>
  (set: HashSet<A>): HashSet<A> => {
    concrete(set)
    return modifyHash(set, value, set.config.hash(value), () => O.none)
  }

export const size = <A>(set: HashSet<A>): number => {
  concrete(set)
  return set.size
}

export const union =
  <A>(that: HashSet<A>) =>
  (set: HashSet<A>): HashSet<A> => {
    concrete(set)
    concrete(that)
    let shorter: HashSet<A>
    let longer: HashSet<A>
    if (size(set) > size(that)) {
      shorter = that
      longer = set
    } else {
      shorter = set
      longer = that
    }
    const result = beginMutation(longer)
    pipe(
      shorter,
      forEach((a) => {
        add(a)(result)
      }),
    )
    return endMutation(result)
  }

export const intersection =
  <A>(that: HashSet<A>) =>
  (set: HashSet<A>) => {
    let shorter: HashSet<A>
    let longer: HashSet<A>
    if (size(set) > size(that)) {
      shorter = that
      longer = set
    } else {
      shorter = set
      longer = that
    }
    concrete(set)
    const result = beginMutation(make<A>(set.config))
    pipe(
      shorter,
      forEach((a) => {
        if (has(a)(longer)) {
          add(a)(result)
        }
      }),
    )
    return endMutation(result)
  }

export const reduce =
  <A, B>(b: B, f: (b: B, a: A) => B) =>
  (set: HashSet<A>): B => {
    concrete(set)
    const root = set.root
    if (root._tag === "LeafNode") {
      return O.isSome(root.value) ? f(b, root.key) : b
    }
    if (root._tag === "EmptyNode") {
      return b
    }
    const toVisit = [root.children]
    let children: Array<Node<A, void>> | undefined
    while ((children = toVisit.pop())) {
      for (let i = 0, len = children.length; i < len; ) {
        const child = children[i++]
        if (child !== undefined && !isEmptyNode(child)) {
          if (child._tag === "LeafNode") {
            if (O.isSome(child.value)) {
              b = f(b, child.key)
            }
          } else {
            toVisit.push(child.children)
          }
        }
      }
    }
    return b
  }

export const forEach = <A>(f: (a: A) => void): ((set: HashSet<A>) => void) =>
  reduce(undefined as void, (_, a) => f(a))

export const beginMutation = <A>(set: HashSet<A>): HashSet<A> => {
  concrete(set)
  return new HashSetInternal(true, set.edit + 1, set.root, set.size, set.config)
}

export const endMutation = <A>(set: HashSet<A>): HashSet<A> => {
  concrete(set)
  set.editable = false
  return set
}

export const mutate =
  <A>(f: (set: HashSet<A>) => void) =>
  (set: HashSet<A>): HashSet<A> => {
    const transient = beginMutation(set)
    f(transient)
    return endMutation(transient)
  }

export const difference =
  <A>(that: HashSet<A>) =>
  (set: HashSet<A>): HashSet<A> => {
    let shorter: HashSet<A>
    let longer: HashSet<A>
    if (size(set) > size(that)) {
      shorter = that
      longer = set
    } else {
      shorter = set
      longer = that
    }
    const result = beginMutation(longer)
    pipe(
      shorter,
      forEach((a) => {
        remove(a)(result)
      }),
    )
    return endMutation(result)
  }

export const makeDefault = <A>(): HashSet<A> => make(getDefaultDeep())

export const make = <A>(config: HashEq<A>): HashSet<A> =>
  new HashSetInternal(false, 0, new EmptyNode(), 0, config)

export const fromReadonlyArray = <A>(as: ReadonlyArray<A>): HashSet<A> => {
  const result = beginMutation(makeDefault<A>())
  as.forEach((a) => {
    add(a)(result)
  })
  return endMutation(result)
}

export const isSubset =
  <A>(that: HashSet<A>) =>
  (set: HashSet<A>): boolean => {
    if (size(set) > size(that)) {
      return false
    }
    for (const a of set) {
      if (!has(a)(that)) {
        return false
      }
    }
    return true
  }

export const map = <A, B>(f: (a: A) => B): ((set: HashSet<A>) => HashSet<B>) =>
  flow(
    reduce(beginMutation(makeDefault<B>()), (b, a) => add(f(a))(b)),
    endMutation,
  )

export const toReadonlyArray = <A>(set: HashSet<A>): ReadonlyArray<A> => {
  const out: Array<A> = []
  for (const value of set) {
    out.push(value)
  }
  return out
}

export const toReadonlyArraySorted =
  <A>(O: Ord<A>) =>
  (set: HashSet<A>): ReadonlyArray<A> => {
    const out: Array<A> = []
    for (const value of set) {
      out.push(value)
    }
    return out.sort(O.compare)
  }

/**
 * @internal
 */
export function setTree<A>(
  hashSet: HashSet<A>,
  newRoot: Node<A, void>,
  newSize: number,
): HashSet<A> {
  concrete(hashSet)
  if (hashSet.editable) {
    hashSet.root = newRoot
    hashSet.size = newSize
    return hashSet
  }
  return newRoot === hashSet.root
    ? hashSet
    : new HashSetInternal(hashSet.editable, hashSet.edit, newRoot, newSize, hashSet.config)
}

export function modifyHash<A>(
  hashSet: HashSet<A>,
  value: A,
  hash: number,
  f: UpdateFn<void>,
): HashSet<A> {
  concrete(hashSet)
  const size = { value: hashSet.size }
  const newRoot = hashSet.root.modify(
    hashSet.editable ? hashSet.edit : NaN,
    hashSet.config.equals,
    0,
    f,
    hash,
    value,
    size,
  )
  return setTree(hashSet, newRoot, size.value)
}

/**
 * @internal
 */
export function getHash<A>(hashMap: HashSet<A>, key: A, hash: number): O.Option<void> {
  concrete(hashMap)
  let node = hashMap.root
  let shift = 0
  while (true) {
    switch (node._tag) {
      case "LeafNode": {
        return hashMap.config.equals(key, node.key) ? node.value : O.none
      }
      case "CollisionNode": {
        if (hash === node.hash) {
          const children = node.children
          for (const child of children) {
            if ("key" in child && hashMap.config.equals(key, child.key)) {
              return child.value
            }
          }
        }
        return O.none
      }
      case "IndexedNode": {
        const frag = hashFragment(shift, hash)
        const bit = toBitmap(frag)
        if (node.mask & bit) {
          node = node.children[fromBitmap(node.mask, bit)]
          shift += SIZE
          break
        }
        return O.none
      }
      case "ArrayNode": {
        node = node.children[hashFragment(shift, hash)]
        if (node !== undefined) {
          shift += SIZE
          break
        }
        return O.none
      }
      default: {
        return O.none
      }
    }
  }
}
