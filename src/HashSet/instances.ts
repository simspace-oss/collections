import type { Set1 } from "../Set"
import type { HashSet } from "./definition"

import { getDefaultDeep, getDefaultShallow, getDefaultStrict } from "../HashEq"
import {
  add,
  difference,
  fromReadonlyArray,
  has,
  intersection,
  isSubset,
  make,
  remove,
  size,
  union,
} from "./api"

export const URI = "@simspace/collections/HashSet"
export type URI = typeof URI

declare module "fp-ts/HKT" {
  interface URItoKind<A> {
    readonly [URI]: HashSet<A>
  }
}

export const StrictSet: Set1<URI> = {
  URI,
  size,
  add,
  has,
  remove,
  union,
  intersection,
  difference,
  isSubset,
  fromReadonlyArray,
  zero: () => make(getDefaultStrict()),
}

export const ShallowSet: Set1<URI> = {
  URI,
  size,
  add,
  has,
  remove,
  union,
  intersection,
  difference,
  isSubset,
  fromReadonlyArray,
  zero: () => make(getDefaultShallow()),
}

export const DeepSet: Set1<URI> = {
  URI,
  size,
  add,
  has,
  remove,
  union,
  intersection,
  difference,
  isSubset,
  fromReadonlyArray,
  zero: () => make(getDefaultDeep()),
}
