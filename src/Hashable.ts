import seedrandom from "seedrandom"

import { isIterable, isObject, isPlainObject } from "./internal/guards"

export interface Hash<A> {
  readonly hash: (a: A) => number
}

export const hashableSymbol = Symbol.for("@simspace/collections/Hashable")

export interface Hashable {
  readonly [hashableSymbol]: () => number
}

export function isHashable(u: object): u is Hashable {
  return hashableSymbol in u
}

const prng = seedrandom(((Math.random() * 4294967296) >>> 0).toString())
const CACHE = new WeakMap<object, number>()
const symbolCache = new Map<symbol, number>()

const seeds = {
  true: randomInt(),
  false: randomInt(),
  string: randomInt(),
  iterable: randomInt(),
  object: randomInt(),
  undefined: randomInt(),
  null: randomInt(),
  NaN: randomInt(),
  negativeInfinity: randomInt(),
  infinity: randomInt(),
  symbol: randomInt(),
}

function isDefined<T>(value: T | undefined): value is T {
  return value !== void 0
}

export function optimizeHash(n: number) {
  return (n & 0xbfffffff) | ((n >>> 1) & 0x40000000)
}

export function array<A>(array: ReadonlyArray<A>): number {
  return optimizeHash(hashArray(array))
}

export function combine(a: number, b: number): number {
  return optimizeHash(combineHash(a, b))
}

export function unknown<A>(u: A): number {
  return optimizeHash(hashUnknown(u))
}

export function number(n: number): number {
  return optimizeHash(hashNumber(n))
}

export function iterable<A>(it: Iterable<A>): number {
  return optimizeHash(hashIterable(it))
}

export function object(value: object): number {
  return optimizeHash(hashObject(value))
}

export function string(value: string): number {
  return optimizeHash(hashString(value))
}

export function symbol(value: symbol): number {
  return optimizeHash(hashSymbol(value))
}

function randomInt() {
  return prng.int32()
}

function hasValueOf(_: unknown): _ is { valueOf: () => unknown } {
  return isObject(_) && typeof _.valueOf === "function" && _.valueOf !== Object.prototype.valueOf
}

function hashUnknown(arg: unknown): number {
  if (arg === null) {
    return seeds.null
  }
  if (hasValueOf(arg)) {
    arg = arg.valueOf()
    if (arg === null) {
      return seeds.null
    }
  }
  switch (typeof arg) {
    case "number":
      return hashNumber(arg)
    case "string":
      return hashString(arg)
    case "function":
      return hashMiscRef(arg)
    case "object":
      return hashObject(arg as object)
    case "boolean":
      return arg === true ? seeds.true : seeds.false
    case "symbol":
      return hashSymbol(arg)
    case "bigint":
      return hashString(arg.toString(10))
    case "undefined":
      return seeds.undefined
  }
}

function combineHash(a: number, b: number): number {
  return (a * 53) ^ b
}

function hashNumber(n: number): number {
  if (n !== n) {
    return seeds.NaN
  }
  if (n === Infinity) {
    return seeds.infinity
  }
  if (n === -Infinity) {
    return seeds.negativeInfinity
  }
  let h = n | 0
  if (h !== n) {
    const dv = new DataView(new ArrayBuffer(8))
    dv.setFloat64(0, n)
    h = hashBytes(new Uint8Array(dv.buffer))
  }
  return h
}

function hashObject(value: object): number {
  let h = CACHE.get(value)
  if (isDefined(h)) {
    return h
  }
  if (isHashable(value)) {
    h = value[hashableSymbol]()
  } else if (isIterable(value)) {
    h = hashIterable(value)
  } else if (isPlainObject(value)) {
    h = hashPlainObject(value)
  } else {
    h = randomInt()
  }
  CACHE.set(value, h)
  return h
}

function hashMiscRef(o: object): number {
  let h = CACHE.get(o)
  if (isDefined(h)) {
    return h
  }
  h = randomInt()
  CACHE.set(o, h)
  return h
}

function hashArray(arr: ReadonlyArray<unknown>): number {
  let h = seeds.iterable
  for (const elem of arr) {
    h = combineHash(h, hashUnknown(elem))
  }
  return h
}

function hashIterable(it: Iterable<unknown>): number {
  let h = seeds.iterable
  const iterator = it[Symbol.iterator]()
  let current: IteratorResult<unknown>
  // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
  while (!(current = iterator.next()).done) {
    h = combineHash(h, unknown(current.value))
  }
  return h
}

function getOwnPropertyKeys(value: Record<PropertyKey, unknown>): ReadonlyArray<string | symbol> {
  return (Object.getOwnPropertyNames(value) as Array<string | symbol>)
    .concat(Object.getOwnPropertySymbols(value))
    .sort()
}

function hashPlainObject(value: Record<PropertyKey, unknown>): number {
  CACHE.set(value, randomInt())
  const keys = getOwnPropertyKeys(value)
  let h = seeds.object
  for (const key of keys) {
    h = combineHash(h, typeof key === "string" ? hashString(key) : hashSymbol(key))
    const kv = value[key]
    if (isObject(kv)) {
      const c = CACHE.get(kv)
      h = c !== undefined ? combineHash(h, c) : combineHash(h, hashUnknown(kv))
    } else {
      h = combineHash(h, hashUnknown(kv))
    }
  }
  return h
}

function hashSymbol(value: symbol): number {
  if (symbolCache.has(value)) {
    return symbolCache.get(value)!
  }
  const h = seeds.symbol ^ randomInt()
  symbolCache.set(value, h)
  return h
}

const table = new Int32Array(16 * 256)
for (let i = 0; i < 16 * 256; i++) {
  table[i] = prng.int32()
}

function hashBytes(value: Uint8Array): number {
  let h = seeds.string
  let i = 0
  for (; i < value.length - 7; i += 8) {
    h ^= table[value[i]]
    h ^= table[0xff + value[i + 1]]
    h ^= table[2 * 0xff + value[i + 2]]
    h ^= table[3 * 0xff + value[i + 3]]
    h ^= table[4 * 0xff + value[i + 4]]
    h ^= table[5 * 0xff + value[i + 5]]
    h ^= table[6 * 0xff + value[i + 6]]
    h ^= table[7 * 0xff + value[i + 7]]
  }
  for (; i < value.length; i++) {
    h ^= table[(i % 8) * 0xff + value[i]]
  }
  return h
}

const encoder = new TextEncoder()

function hashString(s: string): number {
  return hashBytes(encoder.encode(s))
}
