/* eslint-disable no-bitwise */
import type { HashEq } from "../HashEq"
import type { Node } from "../internal/hamt/node"

import { identity } from "fp-ts/function"

import * as Equ from "../Equatable"
import * as H from "../Hashable"
import { hasTypeId } from "../internal/guards"
import { corresponds_ } from "../Iterable"
import { HashMapIterator } from "./iterator"

export const HashMapTypeId = Symbol.for("@simspace/collections/HashMap")
export type HashMapTypeId = typeof HashMapTypeId

export interface HashMap<K, V> extends Iterable<readonly [K, V]>, H.Hashable, Equ.Equatable {
  readonly _typeId: HashMapTypeId
  readonly _K: () => K
  readonly _V: () => V
}

/**
 * @internal
 */
export class HashMapInternal<K, V> implements HashMap<K, V> {
  readonly _typeId: HashMapTypeId = HashMapTypeId
  readonly _K!: () => K
  readonly _V!: () => V

  constructor(
    public editable: boolean,
    public edit: number,
    public root: Node<K, V>,
    public size: number,
    readonly config: HashEq<K>,
  ) {}

  [Symbol.iterator](): Iterator<readonly [K, V]> {
    return new HashMapIterator(this, identity)
  }

  [H.hashableSymbol](): number {
    return H.combine(H.symbol(HashMapTypeId), H.iterable(this))
  }

  [Equ.equatableSymbol](that: unknown): boolean {
    return (
      isHashMap(that) &&
      corresponds_(
        this,
        that,
        ([k0, v0], [k1, v1]) => Equ.strictEqualsUnknown(k0, k1) && Equ.strictEqualsUnknown(v0, v1),
      )
    )
  }
}

function isHashMap(u: unknown): u is HashMapInternal<unknown, unknown> {
  return hasTypeId(u, HashMapTypeId)
}

/**
 * @internal
 */
export function concrete<K, V>(_: HashMap<K, V>): asserts _ is HashMapInternal<K, V> {
  //
}
