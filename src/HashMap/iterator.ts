import type { TraversalFn, VisitResult } from "../internal/hamt/iterator"
import type { HashMapInternal } from "./definition"

import * as O from "fp-ts/Option"

import { applyCont, visitLazy } from "../internal/hamt/iterator"

/**
 * @internal
 */
export class HashMapIterator<K, V, A> implements IterableIterator<A> {
  v: O.Option<VisitResult<K, V, A>>
  constructor(readonly map: HashMapInternal<K, V>, readonly f: TraversalFn<K, V, A>) {
    this.v = visitLazy(this.map.root, this.f, undefined)
  }

  next(): IteratorResult<A> {
    if (O.isNone(this.v)) {
      return { done: true, value: undefined }
    }
    const v0 = this.v.value
    this.v = applyCont(v0.cont)
    return { done: false, value: v0.value }
  }

  [Symbol.iterator](): IterableIterator<A> {
    return new HashMapIterator(this.map, this.f)
  }
}
