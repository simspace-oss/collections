import type { Map2 } from "../Map"
import type { HashMap } from "./definition"

import { getDefaultDeep, getDefaultShallow, getDefaultStrict } from "../HashEq"
import { fromReadonlyArray, get, has, intersection, make, remove, set, size, union } from "./api"

export const URI = "@simspace/collections/HashMap"
export type URI = typeof URI

declare module "fp-ts/HKT" {
  interface URItoKind2<E, A> {
    readonly [URI]: HashMap<E, A>
  }
}

export const StrictMap: Map2<URI> = {
  URI,
  size,
  has,
  get,
  set,
  remove,
  union,
  intersection,
  zero: () => make(getDefaultStrict()),
  fromReadonlyArray,
}

export const ShallowMap: Map2<URI> = {
  URI,
  size,
  has,
  get,
  set,
  remove,
  union,
  intersection,
  zero: () => make(getDefaultShallow()),
  fromReadonlyArray,
}

export const DeepMap: Map2<URI> = {
  URI,
  size,
  has,
  get,
  set,
  remove,
  union,
  intersection,
  zero: () => make(getDefaultDeep()),
  fromReadonlyArray,
}

export {}
