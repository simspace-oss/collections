/* eslint-disable no-bitwise */
import type { HashEq } from "../HashEq"
import type { Node, UpdateFn } from "../internal/hamt/node"
import type { HashMap } from "./definition"
import type { Ord } from "fp-ts/Ord"
import type { Semigroup } from "fp-ts/Semigroup"

import { flow, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { getDefaultDeep } from "../HashEq"
import { fromBitmap, hashFragment, toBitmap } from "../internal/hamt/bitwise"
import { SIZE } from "../internal/hamt/constants"
import { EmptyNode, isEmptyNode } from "../internal/hamt/node"
import { concrete, HashMapInternal } from "./definition"

/**
 * Mark `map` as mutable.
 */
export const beginMutation = <K, V>(map: HashMap<K, V>): HashMap<K, V> => {
  concrete(map)
  return new HashMapInternal(true, map.edit + 1, map.root, map.size, map.config)
}

/**
 * Mark `map` as immutable.
 */
export const endMutation = <K, V>(map: HashMap<K, V>): HashMap<K, V> => {
  concrete(map)
  map.editable = false
  return map
}

export const fromReadonlyArray = <K, V>(pairs: ReadonlyArray<readonly [K, V]>): HashMap<K, V> => {
  const result = beginMutation(makeDefault<K, V>())
  pairs.forEach(([k, v]) => {
    set(k, v)(result)
  })
  return endMutation(result)
}

export const forEach = <V>(f: (value: V) => void): (<K>(map: HashMap<K, V>) => void) =>
  reduce(undefined as void, (_, v) => f(v))

export const forEachWithIndex = <K, V>(
  f: (key: K, value: V) => void,
): ((map: HashMap<K, V>) => void) => reduceWithIndex(undefined as void, (k, _, v) => f(k, v))

/**
 * Lookup the value for `key` in `map` using internal hash function.
 */
export const get =
  <K>(key: K) =>
  <V>(map: HashMap<K, V>): O.Option<V> => {
    concrete(map)
    return getHash(map, key, map.config.hash(key))
  }

/**
 * Lookup the value for `key` in `map`, returning a boolean describing its existance
 */
export const has = <K>(key: K): (<V>(map: HashMap<K, V>) => boolean) => flow(get(key), O.isSome)

export const intersection =
  <V>(S: Semigroup<V>) =>
  <K>(that: HashMap<K, V>) =>
  (map: HashMap<K, V>): HashMap<K, V> => {
    let shorter: HashMap<K, V>
    let longer: HashMap<K, V>
    if (size(map) > size(that)) {
      shorter = that
      longer = map
    } else {
      shorter = map
      longer = that
    }
    concrete(map)
    const result = beginMutation(make<K, V>(map.config))
    pipe(
      shorter,
      forEachWithIndex((k, v) => {
        const v2 = get(k)(longer)
        if (O.isSome(v2)) {
          set(k, S.concat(v, v2.value))(result)
        }
      }),
    )
    return endMutation(result)
  }

/**
 * Creates a new `HashMap` using the given `HashEq` instance
 */
export const make = <K, V>(config: HashEq<K>): HashMap<K, V> =>
  new HashMapInternal(false, 0, new EmptyNode(), 0, config)

/**
 * Creates a new `HashMap` using the default `HashEq` instance
 */
export const makeDefault = <K, V>(): HashMap<K, V> => make(getDefaultDeep())

export const mapWithIndex =
  <K, A, B>(f: (k: K, a: A) => B) =>
  (map: HashMap<K, A>): HashMap<K, B> => {
    concrete(map)
    return pipe(
      map,
      reduceWithIndex(beginMutation(make<K, B>(map.config)), (k, out, a) => {
        set(k, f(k, a))(out)
        return out
      }),
      endMutation,
    )
  }

export const map = <A, B>(f: (a: A) => B): (<K>(map: HashMap<K, A>) => HashMap<K, B>) =>
  mapWithIndex((_, a) => f(a))

/**
 * Alter the value stored for `key` in `map` using function `f`
 */
export const modify =
  <K, V>(key: K, f: UpdateFn<V>) =>
  (map: HashMap<K, V>): HashMap<K, V> => {
    concrete(map)
    return modifyHash(map, key, map.config.hash(key), f)
  }

/**
 * Mutate `map` within the context of `f`.
 *
 * @note this _will not_ mutate `map`
 */
export const mutate =
  <K, V>(f: (map: HashMap<K, V>) => void) =>
  (map: HashMap<K, V>): HashMap<K, V> => {
    const transient = beginMutation(map)
    f(transient)
    return endMutation(transient)
  }

/**
 * Visit every entry in the map, aggregating data.
 *
 * @note order on nodes is not guaranteed
 */
export const reduceWithIndex =
  <K, V, B>(b: B, f: (k: K, b: B, a: V) => B) =>
  (map: HashMap<K, V>): B => {
    concrete(map)
    const root = map.root
    if (root._tag === "LeafNode") {
      return O.isSome(root.value) ? f(root.key, b, root.value.value) : b
    }
    if (root._tag === "EmptyNode") {
      return b
    }
    const toVisit = [root.children]
    let children: Array<Node<K, V>> | undefined
    while ((children = toVisit.pop())) {
      for (let i = 0, len = children.length; i < len; ) {
        const child = children[i++]
        if (child !== undefined && !isEmptyNode(child)) {
          if (child._tag === "LeafNode") {
            if (O.isSome(child.value)) {
              b = f(child.key, b, child.value.value)
            }
          } else {
            toVisit.push(child.children)
          }
        }
      }
    }
    return b
  }

/**
 * Visit every entry in the map, aggregating data.
 *
 * @note order on nodes is not guaranteed
 */
export const reduce = <V, B>(b: B, f: (b: B, a: V) => B): (<K>(map: HashMap<K, V>) => B) =>
  reduceWithIndex(b, (_, b, v) => f(b, v))

/**
 * Removes the entry for `key` in `map`
 */
export const remove =
  <K>(key: K) =>
  <V>(map: HashMap<K, V>): HashMap<K, V> =>
    modify(key, () => O.none as O.Option<V>)(map)

/**
 * Store `value` for `key` in `map`.
 */
export const set = <K, V>(key: K, value: V): ((map: HashMap<K, V>) => HashMap<K, V>) =>
  modify(key, () => O.some(value))

export const size = <K, V>(map: HashMap<K, V>): number => {
  concrete(map)
  return map.size
}

export const union =
  <V>(S: Semigroup<V>) =>
  <K>(that: HashMap<K, V>) =>
  (map: HashMap<K, V>): HashMap<K, V> => {
    let shorter: HashMap<K, V>
    let longer: HashMap<K, V>
    if (size(map) > size(that)) {
      shorter = that
      longer = map
    } else {
      shorter = map
      longer = that
    }
    concrete(map)
    const result = beginMutation(longer)
    pipe(
      shorter,
      forEachWithIndex((k, v) => {
        const v1 = get(k)(result)
        if (O.isSome(v1)) {
          set(k, S.concat(v, v1.value))(result)
        } else {
          set(k, v)(result)
        }
      }),
    )
    return endMutation(result)
  }

export const toReadonlyArray = <K, V>(map: HashMap<K, V>): ReadonlyArray<readonly [K, V]> => {
  const out: Array<readonly [K, V]> = []
  for (const [key, value] of map) {
    out.push([key, value])
  }
  return out
}

export const toReadonlyArraySorted =
  <K>(O: Ord<K>) =>
  <V>(map: HashMap<K, V>): ReadonlyArray<readonly [K, V]> => {
    const out: Array<readonly [K, V]> = []
    for (const [key, value] of map) {
      out.push([key, value])
    }
    return out.sort(([k0], [k1]) => O.compare(k0, k1))
  }

export function modifyHash<K, V>(
  hashMap: HashMap<K, V>,
  key: K,
  hash: number,
  f: UpdateFn<V>,
): HashMap<K, V> {
  concrete(hashMap)
  const size = { value: hashMap.size }
  const newRoot = hashMap.root.modify(
    hashMap.editable ? hashMap.edit : NaN,
    hashMap.config.equals,
    0,
    f,
    hash,
    key,
    size,
  )
  return setTree(hashMap, newRoot, size.value)
}

/**
 * @internal
 */
export function setTree<K, V>(
  hashMap: HashMap<K, V>,
  newRoot: Node<K, V>,
  newSize: number,
): HashMap<K, V> {
  concrete(hashMap)
  if (hashMap.editable) {
    hashMap.root = newRoot
    hashMap.size = newSize
    return hashMap
  }
  return newRoot === hashMap.root
    ? hashMap
    : new HashMapInternal(hashMap.editable, hashMap.edit, newRoot, newSize, hashMap.config)
}

/**
 * @internal
 */
export function getHash<K, V>(hashMap: HashMap<K, V>, key: K, hash: number): O.Option<V> {
  concrete(hashMap)
  let node = hashMap.root
  let shift = 0
  while (true) {
    switch (node._tag) {
      case "LeafNode": {
        return hashMap.config.equals(key, node.key) ? node.value : O.none
      }
      case "CollisionNode": {
        if (hash === node.hash) {
          const children = node.children
          for (const child of children) {
            if ("key" in child && hashMap.config.equals(key, child.key)) {
              return child.value
            }
          }
        }
        return O.none
      }
      case "IndexedNode": {
        const frag = hashFragment(shift, hash)
        const bit = toBitmap(frag)
        if (node.mask & bit) {
          node = node.children[fromBitmap(node.mask, bit)]
          shift += SIZE
          break
        }
        return O.none
      }
      case "ArrayNode": {
        node = node.children[hashFragment(shift, hash)]
        if (node !== undefined) {
          shift += SIZE
          break
        }
        return O.none
      }
      default: {
        return O.none
      }
    }
  }
}
