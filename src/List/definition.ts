/* eslint-disable @typescript-eslint/strict-boolean-expressions */
/*
 * This file is ported from
 *
 * Scala (https://www.scala-lang.org)
 *
 * Copyright EPFL and Lightbend, Inc.
 *
 * Licensed under Apache License 2.0
 * (http://www.apache.org/licenses/LICENSE-2.0).
 */

import * as Equ from "../Equatable"
import { hasTypeId } from "../internal/guards"

export const ListTypeId = Symbol.for("@simspace/collections/List")
export type ListTypeId = typeof ListTypeId

export class Cons<A> implements Iterable<A> {
  readonly _typeId: ListTypeId = ListTypeId
  readonly _tag = "Cons"
  constructor(readonly head: A, public tail: List<A>) {}

  [Symbol.iterator](): Iterator<A> {
    let done = false
    let these: List<A> = this
    return {
      next() {
        if (done) {
          return this.return!()
        }
        if (isEmpty(these)) {
          done = true
          return this.return!()
        }
        const value: A = these.head
        these = these.tail
        return { done, value }
      },
      return(value?: unknown) {
        if (!done) {
          done = true
        }
        return { done: true, value }
      },
    }
  }

  [Equ.equatableSymbol](that: unknown): boolean {
    return isList(that) && equalsWith(Equ.strictEqualsUnknown)(that)(this)
  }
}

export class Nil<A> implements Iterable<A> {
  readonly _tag = "Nil";
  [Symbol.iterator](): Iterator<A> {
    return {
      next() {
        return { done: true, value: undefined }
      },
    }
  }
  [Equ.equatableSymbol](that: unknown) {
    if (!isList(that) || that._tag !== "Nil") {
      return false
    }
    return true
  }
}

export function isList(u: unknown): u is List<unknown> {
  return hasTypeId(u, ListTypeId)
}

export const _Nil = new Nil<never>()

export type List<A> = Cons<A> | Nil<A>

export function isEmpty<A>(list: List<A>): list is Nil<A> {
  return list._tag === "Nil"
}

export function isNonEmpty<A>(list: List<A>): list is Cons<A> {
  return list._tag === "Cons"
}

export function length<A>(list: List<A>): number {
  let these = list
  let len = 0
  while (!isEmpty(these)) {
    len += 1
    these = these.tail
  }
  return len
}

export const equalsWith =
  <A>(f: (x: A, y: A) => boolean) =>
  (that: List<A>) =>
  (list: List<A>): boolean => {
    if (list === that) {
      return true
    } else if (length(list) !== length(that)) {
      return false
    } else {
      const i0 = list[Symbol.iterator]()
      const i1 = that[Symbol.iterator]()
      let a: IteratorResult<A>
      let b: IteratorResult<A>
      while (!(a = i0.next()).done && !(b = i1.next()).done) {
        if (!f(a.value, b.value)) {
          return false
        }
      }
      return true
    }
  }
