import type { List } from "../definition"

import * as O from "fp-ts/Option"

import { unsafeTail } from "./unsafeTail"

export function tail<A>(list: List<A>): O.Option<List<A>> {
  return O.fromNullable(unsafeTail(list))
}
