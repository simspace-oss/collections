import type { List } from "../definition"

import { isEmpty } from "../definition"

export function unsafeTail<A>(list: List<A>): List<A> | undefined {
  if (isEmpty(list)) {
    return undefined
  }
  return list.tail
}
