import type { List } from "../definition"

import { isEmpty } from "../definition"

export const reduce =
  <A, B>(b: B, f: (b: B, a: A) => B) =>
  (fa: List<A>): B => {
    let acc = b
    let these = fa
    while (!isEmpty(these)) {
      acc = f(acc, these.head)
      these = these.tail
    }
    return acc
  }
