import type { List } from "../definition"
import type { Eq } from "fp-ts/Eq"
import type { Predicate } from "fp-ts/Predicate"

import { fromEquals } from "fp-ts/Eq"
import { unsafeCoerce } from "fp-ts/function"

import { _Nil, Cons } from "../definition"
import { equalsWith, isEmpty } from "../definition"
import { cons, nil, unsafeHead } from "./core"
import { unsafeTail } from "./unsafeTail"

export const filter =
  <A>(p: Predicate<A>) =>
  (list: List<A>): List<A> =>
    filterCommon_(list, p, false)

export const filterNot =
  <A>(p: Predicate<A>) =>
  (list: List<A>): List<A> =>
    filterCommon_(list, p, true)

export const getEq = <A>(E: Eq<A>): Eq<List<A>> =>
  fromEquals((xs, ys) => equalsWith(E.equals)(ys)(xs))

function noneIn<A>(l: List<A>, p: Predicate<A>, isFlipped: boolean): List<A> {
  while (true) {
    if (isEmpty(l)) {
      return nil()
    } else {
      if (p(l.head) !== isFlipped) {
        return allIn(l, l.tail, p, isFlipped)
      } else {
        l = l.tail
      }
    }
  }
}

function allIn<A>(
  start: List<A>,
  remaining: List<A>,
  p: Predicate<A>,
  isFlipped: boolean,
): List<A> {
  while (true) {
    if (isEmpty(remaining)) {
      return start
    } else {
      if (p(remaining.head) !== isFlipped) {
        remaining = remaining.tail
      } else {
        return partialFill(start, remaining, p, isFlipped)
      }
    }
  }
}

function partialFill<A>(
  origStart: List<A>,
  firstMiss: List<A>,
  p: Predicate<A>,
  isFlipped: boolean,
): List<A> {
  const newHead = new Cons<A>(unsafeHead(origStart)!, _Nil)
  let toProcess = unsafeTail(origStart) as Cons<A>
  let currentLast = newHead

  while (!(toProcess === firstMiss)) {
    const newElem = cons(unsafeHead(toProcess)!, _Nil)
    currentLast.tail = newElem
    currentLast = unsafeCoerce(newElem)
    toProcess = unsafeCoerce(toProcess.tail)
  }

  let next = firstMiss.tail
  let nextToCopy: Cons<A> = unsafeCoerce(next)
  while (!isEmpty(next)) {
    const head = unsafeHead(next)!
    if (p(head) !== isFlipped) {
      next = next.tail
    } else {
      while (!(nextToCopy === next)) {
        const newElem = new Cons(unsafeHead(nextToCopy)!, _Nil)
        currentLast.tail = newElem
        currentLast = newElem
        nextToCopy = unsafeCoerce(nextToCopy.tail)
      }
      nextToCopy = unsafeCoerce(next.tail)
      next = next.tail
    }
  }

  if (!isEmpty(nextToCopy)) {
    currentLast.tail = nextToCopy
  }

  return newHead
}

function filterCommon_<A>(list: List<A>, p: Predicate<A>, isFlipped: boolean): List<A> {
  return noneIn(list, p, isFlipped)
}
