/*
 * This file is ported from
 *
 * Scala (https://www.scala-lang.org)
 *
 * Copyright EPFL and Lightbend, Inc.
 *
 * Licensed under Apache License 2.0
 * (http://www.apache.org/licenses/LICENSE-2.0).
 */

import type { List, Nil } from "../definition"
import type { Ord } from "fp-ts/Ord"
import type { Ordering } from "fp-ts/Ordering"
import type { Predicate } from "fp-ts/Predicate"

import { flow } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { ListBuffer } from "../../ListBuffer"
import { _Nil, Cons, isEmpty, length } from "../definition"

export function nil<A>(): Nil<A> {
  return _Nil
}

export function cons<A>(head: A, tail?: List<A>): Cons<A> {
  return new Cons(head, tail ?? _Nil)
}

export const empty: <A = never>() => List<A> = nil

export function from<A>(prefix: Iterable<A>): List<A> {
  const iter = prefix[Symbol.iterator]()
  let a: IteratorResult<A>
  if (!(a = iter.next()).done) {
    const result = new Cons(a.value, _Nil)
    let curr = result
    while (!(a = iter.next()).done) {
      const temp = new Cons(a.value, _Nil)
      curr.tail = temp
      curr = temp
    }
    return result
  } else {
    return _Nil
  }
}

export const prepend =
  <B>(elem: B) =>
  <A>(list: List<A>): List<A | B> =>
    new Cons<A | B>(elem, list)

export const prependAll =
  <B>(prefix: List<B>) =>
  <A>(list: List<A>): List<A | B> => {
    if (isEmpty(list)) {
      return prefix
    } else if (isEmpty(prefix)) {
      return list
    } else {
      const result = new Cons<A | B>(prefix.head, list)
      let curr = result
      let that = prefix.tail
      while (!isEmpty(that)) {
        const temp = new Cons<A | B>(that.head, list)
        curr.tail = temp
        curr = temp
        that = that.tail
      }
      return result
    }
  }

export function reverse<A>(list: List<A>): List<A> {
  let result: List<A> = nil()
  let these = list
  while (!isEmpty(these)) {
    result = prepend(these.head)(result)
    these = these.tail
  }
  return result
}

export function unsafeHead<A>(list: List<A>): A | undefined {
  if (isEmpty(list)) {
    return undefined
  }
  return list.head
}

export function head<A>(list: List<A>): O.Option<A> {
  return O.fromNullable(unsafeHead(list))
}

export function unsafeLast<A>(list: List<A>): A | undefined {
  if (isEmpty(list)) {
    return undefined
  }
  let these = list
  let scout = list.tail
  while (!isEmpty(scout)) {
    these = scout
    scout = scout.tail
  }
  return these.head
}

export const last: <A>(list: List<A>) => O.Option<A> = flow(unsafeLast, O.fromNullable)

export const forEach =
  <A, U>(f: (a: A) => U) =>
  (list: List<A>): void => {
    let these = list
    while (!isEmpty(these)) {
      f(these.head)
      these = these.tail
    }
  }

export const concat =
  <B>(that: List<B>) =>
  <A>(list: List<A>): List<A | B> =>
    prependAll(list)(that)

export const map =
  <A, B>(f: (a: A) => B) =>
  (fa: List<A>): List<B> => {
    if (isEmpty(fa)) {
      return fa as unknown as List<B>
    } else {
      const h = new Cons(f(fa.head), _Nil)
      let t: Cons<B> = h
      let rest = fa.tail
      while (!isEmpty(rest)) {
        const nx = new Cons(f(rest.head), _Nil)
        t.tail = nx
        t = nx
        rest = rest.tail
      }
      return h
    }
  }

export const chain =
  <A, B>(f: (a: A) => List<B>) =>
  (ma: List<A>): List<B> => {
    let rest = ma
    let h: Cons<B> | undefined
    let t: Cons<B> | undefined
    while (!isEmpty(rest)) {
      let bs = f(rest.head)
      while (!isEmpty(bs)) {
        const nx = new Cons(bs.head, _Nil)
        if (t === undefined) {
          h = nx
        } else {
          t.tail = nx
        }
        t = nx
        bs = bs.tail
      }
      rest = rest.tail
    }
    if (h === undefined) {
      return _Nil
    } else {
      return h
    }
  }

export const exists =
  <A>(p: Predicate<A>) =>
  (list: List<A>): boolean => {
    let these = list
    while (!isEmpty(these)) {
      if (p(these.head)) {
        return true
      }
      these = these.tail
    }
    return false
  }

export const find =
  <A>(p: Predicate<A>) =>
  (list: List<A>): O.Option<A> => {
    let these = list
    while (!isEmpty(these)) {
      if (p(these.head)) {
        return O.some(these.head)
      }
      these = these.tail
    }
    return O.none
  }

export const take =
  (n: number) =>
  <A>(list: List<A>): List<A> => {
    if (isEmpty(list) || n <= 0) {
      return _Nil
    } else {
      const h = new Cons(list.head, _Nil)
      let t = h
      let rest = list.tail
      let i = 1
      while (i < n) {
        if (isEmpty(rest)) {
          return list
        }
        i += 1
        const nx = new Cons(rest.head, _Nil)
        t.tail = nx
        t = nx
        rest = rest.tail
      }
      return h
    }
  }

export const sortWith =
  <A>(compare: (x: A, y: A) => Ordering) =>
  (list: List<A>): List<A> => {
    const len = length(list)
    const b = new ListBuffer<A>()
    if (len === 1) {
      b.append(unsafeHead(list)!)
    } else if (len > 1) {
      const arr = new Array<[number, A]>(len)
      copyToArrayWithIndex(list, arr)
      arr.sort(([i, x], [j, y]) => {
        const c = compare(x, y)
        return c !== 0 ? c : i < j ? -1 : 1
      })
      for (let i = 0; i < len; i++) {
        b.append(arr[i][1])
      }
    }
    return b.toList
  }

export const sort = <A>(O: Ord<A>): ((list: List<A>) => List<A>) => sortWith(O.compare)

function copyToArrayWithIndex<A>(list: List<A>, arr: Array<[number, A]>): void {
  let these = list
  let i = 0
  while (!isEmpty(these)) {
    arr[i] = [i, these.head]
    these = these.tail
    i++
  }
}
