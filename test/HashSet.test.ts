import type { Equatable } from "../src/Equatable"
import type { Hashable } from "../src/Hashable"

import fc from "fast-check"
import { pipe } from "fp-ts/lib/function"
import * as N from "fp-ts/number"
import * as Ord from "fp-ts/Ord"
import * as RA from "fp-ts/ReadonlyArray"

import { EqDeep, EqStrict, equatableSymbol, strictEqualsUnknown } from "../src/Equatable"
import * as H from "../src/Hashable"
import * as HS from "../src/HashSet"
import { isObject } from "../src/internal/guards"
import { isValidSet } from "./utils"

const KeyTypeId = Symbol()
type KeyTypeId = typeof KeyTypeId

class Value implements Equatable, Hashable {
  readonly _typeId: KeyTypeId = KeyTypeId
  constructor(readonly x: string, readonly y: number) {}
  [equatableSymbol](that: unknown) {
    return isKey(that) && this.x === that.x && this.y === that.y
  }
  [H.hashableSymbol](): number {
    return H.combine(H.string(this.x), H.number(this.y))
  }
}

class CollidingValue implements Equatable, Hashable {
  readonly _typeId: KeyTypeId = KeyTypeId
  constructor(readonly x: string, readonly y: number) {}
  [equatableSymbol](that: unknown) {
    return isKey(that) && this.x === that.x && this.y === that.y
  }
  [H.hashableSymbol](): number {
    return -1
  }
}

const isKey = (u: unknown): u is Value => isObject(u) && "_typeId" in u && u._typeId === KeyTypeId

describe("HashSet", () => {
  isValidSet({
    name: "HashSet",
    set: HS.StrictSet,
    arbitraryValue: fc.string(),
    arbitraryValueName: "string",
    eq: EqStrict,
  })
  isValidSet({
    name: "HashSet",
    set: HS.StrictSet,
    arbitraryValue: fc.integer(),
    arbitraryValueName: "integer",
    eq: EqStrict,
  })
  isValidSet({
    name: "HashSet",
    set: HS.StrictSet,
    arbitraryValue: fc.float(),
    arbitraryValueName: "float",
    eq: EqStrict,
  })
  isValidSet({
    name: "HashSet",
    set: HS.DeepSet,
    arbitraryValue: fc.object({ maxKeys: 10 }),
    arbitraryValueName: "object",
    eq: EqDeep,
  })
  isValidSet({
    name: "HashSet",
    set: HS.StrictSet,
    arbitraryValue: fc.string().chain((s) => fc.integer().map((n) => new Value(s, n))),
    arbitraryValueName: "custom",
    eq: EqStrict,
  })
  isValidSet({
    name: "HashSet",
    set: HS.StrictSet,
    arbitraryValue: fc.string().chain((s) => fc.integer().map((n) => new CollidingValue(s, n))),
    arbitraryValueName: "custom (colliding)",
    eq: EqStrict,
  })

  const ordValue = pipe(
    N.Ord,
    Ord.contramap((v: Value) => v.y),
  )

  const set = pipe(
    HS.makeDefault<Value>(),
    HS.add(new Value("1", 1)),
    HS.add(new Value("2", 2)),
    HS.add(new Value("3", 3)),
  )

  const largeCollidingSet = pipe(
    HS.makeDefault<Value>(),
    HS.mutate((set) => {
      for (let i = 0; i < 100; i++) {
        HS.add(new CollidingValue(i.toString(), i))(set)
      }
    }),
  )

  describe("add", () => {
    it("should return the same map if setting a key to the same value it already has", () => {
      const newSet = pipe(set, HS.add(new Value("1", 1)))
      expect(newSet === set).toEqual(true)
    })

    it("should return the same map if setting a key to the same value it already has (colliding)", () => {
      const updated = pipe(largeCollidingSet, HS.add(new CollidingValue("50", 50)))
      expect(updated === largeCollidingSet).toEqual(true)
    })
  })

  describe("reduce", () => {
    it("should accumulate a state over the value of the HashSet", () => {
      expect(
        pipe(
          set,
          HS.reduce(0, (b, a) => b + a.y),
        ),
      ).toEqual(6)
    })
  })
  describe("forEach", () => {
    it("should execute `f` for every value in the map", () => {
      const f = jest.fn<void, [Value]>()
      pipe(set, HS.forEach(f))
      expect(f).toHaveBeenCalledTimes(3)
      expect(
        pipe(
          f.mock.calls,
          RA.map(([n]) => n.y),
          RA.sort(N.Ord),
        ),
      ).toEqual([1, 2, 3])
    })
  })
  describe("mutate", () => {
    it("should allow a transient mutation on a copy of the given HashSet", () => {
      const mutated = pipe(
        set,
        HS.mutate((s) => {
          HS.add(new Value("4", 4))(s)
        }),
        HS.toReadonlyArraySorted(ordValue),
        RA.map((v) => v.y),
      )
      const previous = pipe(
        set,
        HS.toReadonlyArraySorted(ordValue),
        RA.map((v) => v.y),
      )
      expect(mutated).toEqual([1, 2, 3, 4])
      expect(previous).toEqual([1, 2, 3])
    })
  })
  describe("difference", () => {
    it("should return a set that contains values that are in one set, but not the other", () => {
      const different = pipe(
        HS.makeDefault<Value>(),
        HS.add(new Value("1", 1)),
        HS.add(new Value("4", 4)),
      )
      const difference = pipe(
        set,
        HS.difference(different),
        HS.toReadonlyArraySorted(ordValue),
        RA.map((v) => v.y),
      )
      expect(difference).toEqual([2, 3])
    })
  })
  describe("intersection", () => {
    it("should return a set that contains values that are in both one set and the other", () => {
      const set2 = pipe(
        HS.makeDefault<Value>(),
        HS.add(new Value("1", 1)),
        HS.add(new Value("2", 2)),
        HS.add(new Value("4", 4)),
      )
      const intersection = pipe(
        set,
        HS.intersection(set2),
        HS.toReadonlyArraySorted(ordValue),
        RA.map((v) => v.y),
      )
      expect(intersection).toEqual([1, 2])
    })
  })

  describe("Hashable", () => {
    it("should hash to the same value as an equal HashSet", () => {
      const otherSet = pipe(
        HS.makeDefault<Value>(),
        HS.add(new Value("1", 1)),
        HS.add(new Value("2", 2)),
        HS.add(new Value("3", 3)),
      )
      expect(H.unknown(set)).toEqual(H.unknown(otherSet))
    })
    it("should hash to a different value than an inequal HashSet", () => {
      const otherSet = pipe(
        HS.makeDefault<Value>(),
        HS.add(new Value("1", 1)),
        HS.add(new Value("2", 2)),
        HS.add(new Value("3", 3)),
        HS.add(new Value("4", 4)),
      )
      expect(H.unknown(set)).not.toEqual(H.unknown(otherSet))
    })
  })

  describe("Equatable", () => {
    it("should return true for a HashMap with the same keys and values", () => {
      const otherSet = pipe(
        HS.makeDefault<Value>(),
        HS.add(new Value("1", 1)),
        HS.add(new Value("2", 2)),
        HS.add(new Value("3", 3)),
      )
      expect(strictEqualsUnknown(set, otherSet)).toEqual(true)
    })
    it("should return true for a HashMap with different keys and values", () => {
      const otherSet = pipe(
        HS.makeDefault<Value>(),
        HS.add(new Value("1", 1)),
        HS.add(new Value("2", 2)),
        HS.add(new Value("3", 3)),
        HS.add(new Value("4", 4)),
      )
      expect(strictEqualsUnknown(set, otherSet)).toEqual(false)
    })
  })
})
