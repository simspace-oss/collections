import type { Map, Map2, Map2C } from "../src/Map"
// eslint-disable-next-line @typescript-eslint/no-unused-vars -- type-only import, and it's used
import type * as S from "../src/Set"
import type { Eq } from "fp-ts/Eq"
import type { URIS, URIS2 } from "fp-ts/HKT"

import fc from "fast-check"
import { deepEqual } from "fast-equals"
import { constFalse, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as Pr from "fp-ts/Predicate"
import * as RA from "fp-ts/ReadonlyArray"
import * as Str from "fp-ts/string"
import lodash from "lodash"

// eslint-disable-next-line @typescript-eslint/no-unsafe-return
const clone = <A>(a: A): A => lodash.clone(a)

export function isValidMap<M extends URIS2, K>({
  name,
  map,
  arbitraryKeyName,
  arbitraryKey,
}: {
  name: string
  map: Map2C<M, K>
  arbitraryKeyName?: string
  arbitraryKey: fc.Arbitrary<K>
}): void
// eslint-disable-next-line @typescript-eslint/unified-signatures
export function isValidMap<M extends URIS2, K>({
  name,
  map,
  arbitraryKeyName,
  arbitraryKey,
}: {
  name: string
  map: Map2<M>
  arbitraryKeyName?: string
  arbitraryKey: fc.Arbitrary<K>
}): void
export function isValidMap<M, K>({
  name,
  map,
  arbitraryKeyName,
  arbitraryKey,
}: {
  name: string
  map: Map<M>
  arbitraryKeyName?: string
  arbitraryKey: fc.Arbitrary<K>
}): void
export function isValidMap<M, K>({
  name,
  map,
  arbitraryKeyName,
  arbitraryKey,
}: {
  name: string
  map: Map<M>
  arbitraryKeyName?: string
  arbitraryKey: fc.Arbitrary<K>
}): void {
  const Map = map
  const arbKeyTag = arbitraryKeyName !== undefined ? ` [${arbitraryKeyName} keys]` : ""
  const arbKey = arbitraryKey
  const arbString = fc.string()

  describe(`${name + arbKeyTag} is a valid instance of Map`, () => {
    test(`${arbKeyTag}An empty Map has size 0`, () =>
      fc.assert(fc.property(fc.boolean(), () => pipe(Map.zero(), Map.size, (n) => n === 0))))

    test(`When a key/value pair is added to a Map, a value exists for that key in the Map`, () =>
      fc.assert(
        fc.property(
          fc.record({
            key: arbKey,
            value: arbString,
          }),
          ({ key, value }) => pipe(Map.zero<K, string>(), Map.set(key, value), Map.has(clone(key))),
        ),
      ))

    test(`When a key/value pair is added to a Map, that exact value should exist for that key in the Map`, () =>
      fc.assert(
        fc.property(
          fc.record({
            key: arbKey,
            value: arbString,
          }),
          ({ key, value }) =>
            pipe(
              Map.zero<K, string>(),
              Map.set(key, value),
              Map.get(clone(key)),
              O.map((v) => Str.Eq.equals(v, value)),
              O.getOrElse(constFalse),
            ),
        ),
      ))

    test(`Looking up a value in an empty Map will return None`, () =>
      fc.assert(
        fc.property(
          fc.record({
            key: arbString,
          }),
          ({ key }) => pipe(Map.zero<string, string>(), Map.get(key), O.isNone),
        ),
      ))

    test(`Looking up a value in a Map after it has been removed will return None`, () =>
      fc.assert(
        fc.property(
          fc.record({
            key: arbKey,
            value: arbString,
          }),
          ({ key, value }) =>
            pipe(
              Map.zero<K, string>(),
              Map.set(key, value),
              Map.remove(clone(key)),
              Map.get(clone(key)),
              O.isNone,
            ),
        ),
      ))

    test(`Inserting a key/value pair with a unique key increments the size by 1`, () =>
      fc.assert(
        fc.property(
          fc.record({
            keys: structurallyUniqueSet(arbKey),
            value: arbString,
          }),
          ({ keys, value }) =>
            pipe(
              keys,
              RA.reduce(Map.zero<K, string>(), (map, key) => Map.set(key, value)(map)),
              Map.size,
              (n) => n === keys.length,
            ),
        ),
      ))

    test(`Inserting two key/value pairs with the same key will only increment the size by 1`, () =>
      fc.assert(
        fc.property(
          fc.record({
            key: arbKey,
            value1: arbString,
            value2: arbString,
          }),
          ({ key, value1, value2 }) =>
            pipe(
              Map.zero<K, string>(),
              Map.set(key, value1),
              Map.set(clone(key), value2),
              Map.size,
              (n) => n === 1,
            ),
        ),
      ))

    test(`Inserting a value for an existing key will cause the latest value to replace the previous value`, () =>
      fc.assert(
        fc.property(
          fc.record({ key: arbKey, value1: arbString, value2: arbString }),
          ({ key, value1, value2 }) =>
            pipe(
              Map.zero<K, string>(),
              Map.set(key, value1),
              Map.set(clone(key), value2),
              Map.get(clone(key)),
              O.map((v) => v === value2),
              O.getOrElse(constFalse),
            ),
        ),
      ))

    test(`Inserting N number of unique key/value pairs into a Map will produce a Map of size N`, () => {
      fc.assert(
        fc.property(
          fc.record({
            keys: structurallyUniqueSet(arbKey),
            value: arbString,
          }),
          ({ keys, value }) => {
            return pipe(
              keys,
              RA.reduce(Map.zero<K, string>(), (map, key) => Map.set(key, value)(map)),
              Map.size,
              (n) => n === keys.length,
            )
          },
        ),
      )
    })

    test("Calling 'intersection' with 2 Maps produces a Map that includes exactly the set of keys they have in common.", () => {
      fc.assert(
        fc.property(
          fc.record({
            keys1: fc.set(arbKey, 0, 50, deepEqual),
            keys2: fc.set(arbKey, 0, 50, deepEqual),
            value: arbString,
          }),
          ({ keys1, keys2, value }) => {
            const keyIntersection = getArrayIntersection(keys1, keys2)
            const map1 = Map.fromReadonlyArray(keys1.map((key) => [key, value]))
            const map2 = Map.fromReadonlyArray(keys2.map((key) => [key, value]))

            const mapIntersection = pipe(
              map1,
              Map.intersection({
                concat: (a: string, b: string) => (Str.Ord.compare(a, b) < 0 ? a : b),
              })(map2),
            )

            const containsAllOfIntersection = pipe(
              keyIntersection,
              RA.reduce(true, (current, key) =>
                current === true ? pipe(mapIntersection, Map.has(key)) : false,
              ),
            )

            const containsNoMoreThanIntersection = pipe(
              keyIntersection,
              RA.reduce(mapIntersection, (map, key) => Map.remove(key)(map)),
              Map.size,
              (n) => n === 0,
            )

            return containsAllOfIntersection && containsNoMoreThanIntersection
          },
        ),
      )
    })

    test("Calling 'union' with 2 Maps produces a Map with all the unique keys from both Maps", () =>
      fc.assert(
        fc.property(
          fc.record({
            keys1: fc.set(arbKey, 0, 50, deepEqual),
            keys2: fc.set(arbKey, 0, 50, deepEqual),
            value: arbString,
          }),
          ({ keys1, keys2, value }) => {
            const keyUnion = getArrayUnion(keys1, keys2)
            const map1 = Map.fromReadonlyArray(keys1.map((key) => [key, value]))
            const map2 = Map.fromReadonlyArray(keys2.map((key) => [key, value]))

            const mapUnion = pipe(
              map1,
              Map.union({
                concat: (a: string, b: string) => (Str.Ord.compare(a, b) < 0 ? a : b),
              })(map2),
            )

            const containsAllOfUnion = pipe(
              keyUnion,
              RA.reduce(true, (current, key) =>
                current === true ? pipe(mapUnion, Map.has(key)) : false,
              ),
            )

            const containsNoMoreThanUnion = pipe(
              keyUnion,
              RA.reduce(mapUnion, (map, key) => Map.remove(key)(map)),
              Map.size,
              (n) => n === 0,
            )

            return containsAllOfUnion && containsNoMoreThanUnion
          },
        ),
      ))
  })
}

const getArrayUnion = <A>(a1: Array<A>, a2: Array<A>) => lodash.unionWith(a1, a2, deepEqual)

const getArrayIntersection = <A>(a1: Array<A>, a2: Array<A>) =>
  lodash.intersectionWith(a1, a2, deepEqual)

const structurallyUniqueSet = <A>(arb: fc.Arbitrary<A>) => fc.set(arb, deepEqual)

export function isValidSet<S extends URIS, V>(params: {
  name: string
  set: S.Set1<S>
  arbitraryValueName: string
  arbitraryValue: fc.Arbitrary<V>
  eq: Eq<V>
}): void
// eslint-disable-next-line @typescript-eslint/unified-signatures
export function isValidSet<S extends URIS, V>(params: {
  name: string
  set: S.Set1C<S, V>
  arbitraryValueName: string
  arbitraryValue: fc.Arbitrary<V>
  eq: Eq<V>
}): void
export function isValidSet<S, V>(params: {
  name: string
  set: S.Set<S>
  arbitraryValueName: string
  arbitraryValue: fc.Arbitrary<V>
  eq: Eq<V>
}): void
export function isValidSet<S, V>({
  name,
  set,
  arbitraryValueName,
  arbitraryValue,
  eq,
}: {
  name: string
  set: S.Set<S>
  arbitraryValueName: string
  arbitraryValue: fc.Arbitrary<V>
  eq: Eq<V>
}): void {
  const arbValueTag = arbitraryValueName !== undefined ? ` [${arbitraryValueName} keys]` : ""

  describe(`${name + arbValueTag} is a valid instance of Set`, () => {
    test("An empty Set has size 0", () =>
      fc.assert(fc.property(fc.boolean(), () => pipe(set.zero(), set.size, (n) => n === 0))))

    test("When a value is added to a Set, the value exists in the returned Set", () =>
      fc.assert(
        fc.property(arbitraryValue, (value) =>
          pipe(set.zero<V>(), set.add(value), set.has(clone(value))),
        ),
      ))

    test("When a value is added to a Set and then removed, the value does not exist in the returned Set", () =>
      fc.assert(
        fc.property(fc.integer(), (value) =>
          pipe(
            set.zero<number>(),
            set.add(value),
            set.remove(clone(value)),
            Pr.not(set.has(clone(value))),
          ),
        ),
      ))

    test("Inserting N unique values into an empty Set increases the size by N", () =>
      fc.assert(
        fc.property(
          fc
            .tuple(arbitraryValue, arbitraryValue, arbitraryValue, arbitraryValue)
            .filter((t) => RA.uniq(eq)(t).length === 4),
          ([v1, v2, v3, v4]) =>
            pipe(
              set.zero<V>(),
              set.add(v1),
              set.add(v2),
              set.add(v3),
              set.add(v4),
              set.size,
              (n) => n === 4,
            ),
        ),
      ))

    test("Inserting N identical values into an empty Set increases the size by 1", () =>
      fc.assert(
        fc.property(arbitraryValue, (value) =>
          pipe(
            set.zero<V>(),
            set.add(value),
            set.add(clone(value)),
            set.add(clone(value)),
            set.add(clone(value)),
            set.size,
            (n) => n === 1,
          ),
        ),
      ))

    test("A union between 2 identical Sets should be identical to both Sets", () => {
      fc.assert(
        fc.property(fc.array(arbitraryValue), (array) => {
          const set1 = pipe(
            array,
            RA.reduce(set.zero<V>(), (s, value) => set.add(clone(value))(s)),
          )

          const set2 = pipe(
            array,
            RA.reduce(set.zero<V>(), (s, value) => set.add(clone(value))(s)),
          )

          const unionSet = set.union(set1)(set2)

          return (
            set.isSubset(unionSet)(set1) &&
            set.isSubset(unionSet)(set2) &&
            set.isSubset(set1)(unionSet) &&
            set.isSubset(set2)(unionSet)
          )
        }),
      )
    })

    test("A union between two Sets is a subset of both Sets", () =>
      fc.assert(
        fc.property(
          fc.record({
            array1: fc.array(arbitraryValue),
            array2: fc.array(arbitraryValue),
          }),
          ({ array1, array2 }) => {
            const set1 = pipe(
              array1,
              RA.reduce(set.zero<V>(), (s, value) => set.add(clone(value))(s)),
            )

            const set2 = pipe(
              array2,
              RA.reduce(set.zero<V>(), (s, value) => set.add(clone(value))(s)),
            )

            const unionSet = set.union(set1)(set2)

            return set.isSubset(unionSet)(set1) && set.isSubset(unionSet)(set2)
          },
        ),
      ))

    test("Inserting N number of unique values into a Set will produce a Set of size N", () =>
      fc.assert(
        fc.property(fc.set(arbitraryValue, 1, 10, eq.equals), (values) =>
          pipe(
            values,
            RA.reduce(set.zero<V>(), (s0, value) => set.add(clone(value))(s0)),
            set.size,
            (n) => n === values.length,
          ),
        ),
      ))

    test("Calling 'union' with 2 Sets produces a Set with all the unique values from both Sets", () =>
      fc.assert(
        fc.property(
          fc.record({
            values1: fc.set(arbitraryValue, 0, 50, deepEqual),
            values2: fc.set(arbitraryValue, 0, 50, deepEqual),
          }),
          ({ values1, values2 }) => {
            const keyUnion = getArrayUnion(values1, values2)
            const set1 = set.fromReadonlyArray(values1)
            const set2 = set.fromReadonlyArray(values2)

            const setUnion = pipe(set1, set.union(set2))

            const containsAllOfUnion = pipe(
              keyUnion,
              RA.reduce(true, (current, key) =>
                current === true ? pipe(setUnion, set.has(key)) : false,
              ),
            )

            const containsNoMoreThanUnion = pipe(
              keyUnion,
              RA.reduce(setUnion, (map, key) => set.remove(key)(map)),
              set.size,
              (n) => n === 0,
            )

            return containsAllOfUnion && containsNoMoreThanUnion
          },
        ),
      ))
  })
}
