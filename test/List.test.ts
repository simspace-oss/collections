import { identity, pipe } from "fp-ts/lib/function"
import * as N from "fp-ts/number"
import * as O from "fp-ts/Option"
import * as Ord from "fp-ts/Ord"

import * as Equ from "../src/Equatable"
import * as L from "../src/List"

const numbers = L.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

describe("List", () => {
  describe("empty", () => {
    it("returns the empty List", () => {
      const l = L.empty()
      expect(L.length(l)).toEqual(0)
      expect(l).toEqual(L.from([]))
    })
  })

  describe("from", () => {
    it("returns a new List from an Iterable", () => {
      let l = L.empty<number>()
      l = L.prepend(2)(l)
      l = L.prepend(1)(l)
      l = L.prepend(0)(l)
      expect(L.from([0, 1, 2])).toEqual(l)
    })
  })

  describe("length", () => {
    it("returns the number of elements in the List", () => {
      let l = L.empty<number>()
      for (let i = 0; i < 10; i++) {
        l = L.prepend(i)(l)
      }
      expect(L.length(l)).toEqual(10)
    })
  })

  describe("Iterable", () => {
    it("should iterate through each element of a List in order", () => {
      const expected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
      const actual: Array<number> = []
      for (const n of numbers) {
        actual.push(n)
      }
      expect(expected).toEqual(actual)
    })
  })

  describe("equalsWith", () => {
    it("cpompares two lists based on the given equality function", () => {
      const l0 = L.from([{ n: 0 }, { n: 1 }, { n: 2 }])
      const l1 = L.from([{ n: 0 }, { n: 1 }, { n: 2 }])
      const l2 = L.from([{ n: 1 }, { n: 2 }, { n: 3 }])

      const equalsFn = (x: { n: number }, y: { n: number }): boolean => x.n === y.n

      expect(L.equalsWith(equalsFn)(l1)(l0)).toEqual(true)
      expect(L.equalsWith(equalsFn)(l2)(l1)).toEqual(false)
    })
  })

  describe("prepend", () => {
    it("returns a new List with an element prepended", () => {
      let l = L.empty<number>()
      for (let i = 0; i < 10; i++) {
        l = L.prepend(i)(l)
      }
      expect(l).toEqual(L.from([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]))
    })
  })

  describe("prependAll", () => {
    it("returns a new List with another List prepended", () => {
      const prefix = L.from([4, 3, 2, 1])
      let l = L.empty<number>()
      l = L.prepend(0)(l)
      l = L.prependAll(prefix)(l)
      expect(Equ.strictEqualsUnknown(l, L.from([4, 3, 2, 1, 0]))).toEqual(true)
    })
    it("returns the original List if one is empty", () => {
      expect(L.prependAll(L.empty())(numbers) === numbers).toEqual(true)
      expect(L.prependAll(numbers)(L.empty()) === numbers).toEqual(true)
    })
  })

  describe("map", () => {
    it("applies a function to each element of a List, returning a new List of the results", () => {
      const l = L.from([1, 2, 3, 4])
      const mapped = L.map((n: number) => n * 2)(l)
      expect(mapped).toEqual(L.from([2, 4, 6, 8]))
    })
    it("should return an empty List if the list is empty", () => {
      expect(L.map(identity)(L.nil())).toEqual(L.nil())
    })
  })

  describe("sortWith", () => {
    it("returns a new List with the elements sorted according to the given compare function", () => {
      const l = L.sortWith((x: number, y: number) => (x < y ? -1 : x > y ? 1 : 0))(
        L.from([9, 4, 6, 2, 7, 3, 5, 1, 8, 0]),
      )
      expect(l).toEqual(numbers)
    })

    it("should be a stable sort", () => {
      const l0 = L.from([
        { x: 1, y: "first" },
        { x: 2, y: "second" },
        { x: 1, y: "third" },
        { x: 1.5, y: "fourth" },
        { x: 0, y: "fifth" },
      ])
      expect(
        pipe(
          l0,
          L.sort(
            pipe(
              N.Ord,
              Ord.contramap(({ x }) => x),
            ),
          ),
        ),
      ).toEqual(
        L.from([
          { x: 0, y: "fifth" },
          { x: 1, y: "first" },
          { x: 1, y: "third" },
          { x: 1.5, y: "fourth" },
          { x: 2, y: "second" },
        ]),
      )
    })

    it("should return a List with a single element if given a List with a single element", () => {
      expect(L.sortWith((x: number, y: number) => (x > y ? 1 : x < y ? -1 : 0))(L.cons(1))).toEqual(
        L.cons(1),
      )
    })
  })

  describe("filter", () => {
    it("returns a new List filtered with the given predicate", () => {
      const l = L.filter((n: number) => n % 2 === 0)(numbers)
      expect(l).toEqual(L.from([0, 2, 4, 6, 8]))
    })
  })

  describe("filterNow", () => {
    it("returns a new List filtered with the inverse of the given predicate", () => {
      const l = L.filterNot((n: number) => n % 2 === 0)(numbers)
      expect(l).toEqual(L.from([1, 3, 5, 7, 9]))
    })
  })

  describe("exists", () => {
    it("returns true if any element satisfies the given predicate", () => {
      expect(L.exists((n: number) => n === 5)(numbers)).toEqual(true)
    })
    it("returns false if no element satisfies the given predicate", () => {
      expect(L.exists((n: number) => n === 100)(numbers)).toEqual(false)
    })
  })

  describe("find", () => {
    it("returns the first element matching the given predicate", () => {
      expect(L.find((n: number) => n > 5)(numbers)).toEqual(O.some(6))
    })
    it("returns None if no element matches the given predicate", () => {
      expect(L.find((n: number) => n < 0)(numbers)).toEqual(O.none)
    })
  })

  describe("unsafeHead", () => {
    it("returns the first element of a List", () => {
      expect(L.unsafeHead(numbers)).toEqual(0)
    })
    it("returns undefined if the List is empty", () => {
      expect(L.unsafeHead(L.empty())).toEqual(undefined)
    })
  })

  describe("head", () => {
    it("returns the first element of a List in an Option", () => {
      expect(L.head(numbers)).toEqual(O.some(0))
    })
    it("returns None if the List is empty", () => {
      expect(L.head(L.empty())).toEqual(O.none)
    })
  })

  describe("unsafeTail", () => {
    it("returns the List with the first element excluded", () => {
      expect(L.unsafeTail(numbers)).toEqual(L.from([1, 2, 3, 4, 5, 6, 7, 8, 9]))
    })
    it("returns undefined if the List is empty", () => {
      expect(L.unsafeTail(L.empty())).toEqual(undefined)
    })
  })

  describe("tail", () => {
    it("returns the List with the first element excluded in an Option", () => {
      expect(L.tail(numbers)).toEqual(O.some(L.from([1, 2, 3, 4, 5, 6, 7, 8, 9])))
    })
    it("returns None if the List is empty", () => {
      expect(L.tail(L.empty())).toEqual(O.none)
    })
  })

  describe("unsafeLast", () => {
    it("returns the last element in the List", () => {
      expect(L.unsafeLast(numbers)).toEqual(9)
    })
    it("returns undefined if the list is empty", () => {
      expect(L.unsafeLast(L.empty())).toEqual(undefined)
    })
  })

  describe("last", () => {
    it("returns the last element in the List in an Option", () => {
      expect(L.last(numbers)).toEqual(O.some(9))
    })
    it("returns None if the list is empty", () => {
      expect(L.last(L.empty())).toEqual(O.none)
    })
  })

  describe("reverse", () => {
    it("returns a new List with the order of elements reversed", () => {
      expect(L.reverse(numbers)).toEqual(L.from([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]))
    })
  })

  describe("take", () => {
    it("returns a new List with the given number of elements, taken from the beginning", () => {
      expect(L.take(5)(numbers)).toEqual(L.from([0, 1, 2, 3, 4]))
    })
    it("returns the original List if the given number of elements is greater than the length", () => {
      expect(L.take(11)(numbers) === numbers).toEqual(true)
    })
    it("returns the empty List if the list is empty", () => {
      expect(L.take(0)(numbers)).toEqual(L.empty())
    })
  })

  describe("reduce", () => {
    it("accumulates a value over a List", () => {
      expect(L.reduce(0, (b, a: number) => b + a)(numbers)).toEqual(45)
    })
    it("returns the initial value if the list is empty", () => {
      expect(L.reduce(0, (b, a: number) => b + a)(L.empty())).toEqual(0)
    })
  })

  describe("concat", () => {
    it("concatenates two Lists", () => {
      expect(L.concat(L.from([10, 11, 12, 13, 14, 15]))(numbers)).toEqual(
        L.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]),
      )
    })
    it("returns the original List if the other one is empty", () => {
      expect(L.concat(numbers)(L.empty()) === numbers).toEqual(true)
      expect(L.concat(L.empty())(numbers) === numbers).toEqual(true)
    })
  })

  describe("forEach", () => {
    it("calls the provided function with each element", () => {
      let acc = 0

      const f = jest.fn((n: number) => {
        acc += n
      })

      L.forEach(f)(numbers)

      expect(f).toHaveBeenCalledTimes(10)
      expect(acc).toEqual(45)
    })
  })

  describe("chain", () => {
    it("concatenates each returned list into the result", () => {
      const l = L.from([0, 1, 2])
      expect(L.chain((n: number) => L.from([n, n * 2, n * 3]))(l)).toEqual(
        L.from([0, 0, 0, 1, 2, 3, 2, 4, 6]),
      )
    })
    it("should return an empty list if empty", () => {
      expect(L.chain(L.cons)(L.nil())).toEqual(L.nil())
    })
  })

  // describe('builder', () => {
  //   it('mutably constructs a List', () => {
  //     const b = L.builder<number>()
  //     b.append(0)
  //     b.append(1)
  //     b.append(2)
  //     expect(b.build()).toEqual(L.from([0, 1, 2]))
  //   })

  //   it('returns the empty List if no element is appended', () => {
  //     const b = L.builder<number>()
  //     expect(b.build()).toEqual(L.empty())
  //   })
  // })
})
