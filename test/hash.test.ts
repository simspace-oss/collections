import fc from "fast-check"
import * as Eq from "fp-ts/Eq"
import { pipe } from "fp-ts/function"
import * as N from "fp-ts/number"
import * as RA from "fp-ts/ReadonlyArray"
import * as Str from "fp-ts/string"

import { EqDeep, EqStrict } from "../src/Equatable"
import * as H from "../src/Hashable"

const arbitrarySymbol = fc.constant(() => Symbol()).map((f) => f())

class NonObjectConstructor {}

describe("hash", () => {
  describe("hashArray", () => {
    assertHash("Array", fc.array(fc.integer()), RA.getEq(N.Eq), [], H.array)
  })
  describe("hashString", () => {
    assertHash("string", fc.string(), Str.Eq, "", H.string)
  })
  describe("hashNumber", () => {
    assertHash("integer", fc.integer(), N.Eq, 0, H.number)
    assertHash(
      "float",
      fc.float({ next: true, noNaN: true, noDefaultInfinity: true }),
      N.Eq,
      NaN,
      H.number,
    )
    it("should return a constant for Infinity", () => {
      expect(H.number(Infinity)).toEqual(H.number(Infinity))
      expect(H.number(-Infinity)).toEqual(H.number(-Infinity))
    })
    it("should return a constant for NaN", () => {
      expect(H.number(NaN)).toEqual(H.number(NaN))
      expect(H.number(-NaN)).toEqual(H.number(-NaN))
      expect(H.number(NaN)).toEqual(H.number(-NaN))
      expect(H.number(-NaN)).toEqual(H.number(NaN))
    })
    it("should return 0 for -0", () => {
      expect(H.number(-0)).toEqual(0)
    })
  })
  describe("hashObject", () => {
    assertHash("object", fc.object(), EqDeep, {}, H.object)
  })
  describe("hashMiscRef", () => {
    assertHash("function", fc.func(fc.integer()), EqStrict, () => 0, H.unknown)
  })
  describe("hashIterable", () => {
    assertHash(
      "iterable",
      fc.array(fc.string()).map((arr) => arr as Iterable<string>),
      pipe(
        RA.getEq(Str.Eq),
        Eq.contramap((iter) => Array.from(iter)),
      ),
      [],
      H.iterable,
    )
  })
  describe("hashSymbol", () => {
    assertHash("symbol", arbitrarySymbol, EqStrict, Symbol(), H.symbol)
  })
  describe("hashUnknown", () => {
    assertHash("unknown", fc.anything(), EqDeep, {}, H.unknown)
    assertHash(
      "unknown",
      fc.object({ key: fc.string(), values: [arbitrarySymbol, fc.bigUint()] }),
      EqDeep,
      {},
      H.unknown,
    )
    assertHash(
      "NonObjectConstructor",
      fc.constant(() => new NonObjectConstructor()).map((f) => f()),
      EqStrict,
      {},
      H.unknown,
    )
  })
})

function assertHash<A>(
  name: string,
  arb: fc.Arbitrary<A>,
  eq: Eq.Eq<A>,
  empty: A,
  hashFn: (a: A) => number,
) {
  it(`should return the same hash for the "empty" ${name}`, () => {
    expect(hashFn(empty)).toEqual(hashFn(empty))
  })
  it(`should return the same hash for the same ${name}s`, () => {
    fc.assert(fc.property(arb, (a) => hashFn(a) === hashFn(a)))
  })
  it(`should return a different hash for different ${name}s`, () => {
    fc.assert(
      fc.property(different(arb, eq), ([a0, a1]) => expect(hashFn(a0)).not.toEqual(hashFn(a1))),
    )
  })
}

function different<A>(arb: fc.Arbitrary<A>, eq: Eq.Eq<A>): fc.Arbitrary<readonly [A, A]> {
  return fc
    .tuple(arb, arb)
    .chain(([a0, a1]) =>
      eq.equals(a0, a1) ? different(arb.noShrink(), eq) : fc.constant([a0, a1]),
    )
}
