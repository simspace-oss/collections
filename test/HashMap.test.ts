import type { Equatable } from "../src/Equatable"
import type { Hashable } from "../src/Hashable"

import fc from "fast-check"
import { pipe } from "fp-ts/function"
import * as N from "fp-ts/number"
import * as O from "fp-ts/Option"
import * as Ord from "fp-ts/Ord"
import * as RA from "fp-ts/ReadonlyArray"

import { equatableSymbol, strictEqualsUnknown } from "../src/Equatable"
import * as H from "../src/Hashable"
import * as HM from "../src/HashMap"
import { isObject } from "../src/internal/guards"
import { isValidMap } from "./utils"

const KeyTypeId = Symbol()
type KeyTypeId = typeof KeyTypeId

class Key implements Equatable, Hashable {
  readonly _typeId: KeyTypeId = KeyTypeId
  constructor(readonly x: string, readonly y: number) {}
  [equatableSymbol](that: unknown) {
    return isKey(that) && this.x === that.x && this.y === that.y
  }
  [H.hashableSymbol](): number {
    return H.combine(H.string(this.x), H.number(this.y))
  }
}

class CollidingKey implements Equatable, Hashable {
  readonly _typeId: KeyTypeId = KeyTypeId
  constructor(readonly x: string, readonly y: number) {}
  [equatableSymbol](that: unknown) {
    return isKey(that) && this.x === that.x && this.y === that.y
  }
  [H.hashableSymbol](): number {
    return -1
  }
}

const isKey = (u: unknown): u is Key => isObject(u) && "_typeId" in u && u._typeId === KeyTypeId

describe("HashMap", () => {
  isValidMap({
    name: "HashMap",
    map: HM.StrictMap,
    arbitraryKey: fc.string(),
    arbitraryKeyName: "string",
  })
  isValidMap({
    name: "HashMap",
    map: HM.StrictMap,
    arbitraryKey: fc.integer(),
    arbitraryKeyName: "integer",
  })
  isValidMap({
    name: "HashMap",
    map: HM.StrictMap,
    arbitraryKey: fc.float(),
    arbitraryKeyName: "float",
  })
  isValidMap({
    name: "HashMap",
    map: HM.DeepMap,
    arbitraryKey: fc.object({ maxKeys: 10 }),
    arbitraryKeyName: "object",
  })
  isValidMap({
    name: "HashMap",
    map: HM.StrictMap,
    arbitraryKey: fc.string().chain((s) => fc.integer().map((n) => new Key(s, n))),
    arbitraryKeyName: "custom",
  })
  isValidMap({
    name: "HashMap",
    map: HM.StrictMap,
    arbitraryKey: fc.string().chain((s) => fc.integer().map((n) => new CollidingKey(s, n))),
    arbitraryKeyName: "custom (colliding)",
  })

  const map = pipe(
    HM.makeDefault<Key, number>(),
    HM.set(new Key("1", 1), 1),
    HM.set(new Key("2", 2), 2),
    HM.set(new Key("3", 3), 3),
  )

  const largeMap = pipe(
    HM.makeDefault<Key, number>(),
    HM.mutate((map) => {
      for (let i = 0; i < 100; i++) {
        HM.set(new Key(i.toString(), i), i)(map)
      }
    }),
  )

  const largeCollidingMap = pipe(
    HM.makeDefault<Key, number>(),
    HM.mutate((map) => {
      for (let i = 0; i < 100; i++) {
        HM.set(new CollidingKey(i.toString(), i), i)(map)
      }
    }),
  )

  describe("HashMapIterator", () => {
    it("should iterate through all key-value pairs", () => {
      const expected = [
        [new Key("1", 1), 1],
        [new Key("2", 2), 2],
        [new Key("3", 3), 3],
      ]
      const actual: Array<readonly [Key, number]> = []
      for (const [key, value] of map) {
        actual.push([key, value])
      }
      expect(
        actual.sort(
          pipe(
            N.Ord,
            Ord.contramap((k: readonly [Key, number]) => k[0].y),
          ).compare,
        ),
      ).toEqual(expected)
    })
  })

  describe("set", () => {
    it("should return the same map if setting a key to the same value it already has", () => {
      const newMap = pipe(map, HM.set(new Key("1", 1), 1))
      expect(newMap === map).toEqual(true)
    })

    it("should return the same map if setting a key to the same value it already has (colliding)", () => {
      const updated = pipe(largeCollidingMap, HM.set(new CollidingKey("50", 50), 50))
      expect(updated === largeCollidingMap).toEqual(true)
    })

    it("should update values of keys with colliding hashes", () => {
      const updated = pipe(largeCollidingMap, HM.set(new CollidingKey("50", 50), 1000))

      expect(HM.get(new CollidingKey("50", 50))(largeCollidingMap)).toEqual(O.some(50))
      expect(HM.get(new CollidingKey("50", 50))(updated)).toEqual(O.some(1000))
    })
  })

  describe("modify", () => {
    it("should return the same map if the key does not exist, and the value is None", () => {
      const updated = pipe(
        map,
        HM.modify(new Key("4", 4), () => O.none as O.Option<number>),
      )
      expect(updated === map).toEqual(true)
    })
  })

  describe("remove", () => {
    it("should remove a key-value pair from a large map", () => {
      const removed50 = pipe(largeMap, HM.remove(new Key("50", 50)))

      expect(HM.has(new Key("50", 50))(largeMap)).toEqual(true)
      expect(HM.has(new Key("50", 50))(removed50)).toEqual(false)
    })

    it("should remove a key-value pair from a large map (colliding)", () => {
      const removed50 = pipe(largeMap, HM.remove(new CollidingKey("50", 50)))

      expect(HM.has(new CollidingKey("50", 50))(largeCollidingMap)).toEqual(true)
      expect(HM.has(new CollidingKey("50", 50))(removed50)).toEqual(false)
    })
  })

  describe("forEach", () => {
    it("should execute `f` for every value in the map", () => {
      const f = jest.fn<void, [number]>()
      pipe(map, HM.forEach(f))
      expect(f).toHaveBeenCalledTimes(3)
      expect(
        pipe(
          f.mock.calls,
          RA.map(([n]) => n),
          RA.sort(N.Ord),
        ),
      ).toEqual([1, 2, 3])
    })
  })

  describe("map", () => {
    it("should transform each value in the map using function `f`", () => {
      const transformed = pipe(
        map,
        HM.map((n) => n.toString(10)),
        HM.toReadonlyArraySorted(
          pipe(
            N.Ord,
            Ord.contramap((key) => key.y),
          ),
        ),
      )
      const expected = [
        [new Key("1", 1), "1"],
        [new Key("2", 2), "2"],
        [new Key("3", 3), "3"],
      ]
      expect(transformed).toEqual(expected)
    })
  })

  describe("Hashable", () => {
    it("should hash to the same value as an equal HashMap", () => {
      const otherMap = pipe(
        HM.makeDefault<Key, number>(),
        HM.set(new Key("1", 1), 1),
        HM.set(new Key("2", 2), 2),
        HM.set(new Key("3", 3), 3),
      )
      expect(H.unknown(map)).toEqual(H.unknown(otherMap))
    })
    it("should hash to a different value than an inequal HashMap", () => {
      const otherMap = pipe(
        HM.makeDefault<Key, number>(),
        HM.set(new Key("1", 1), 1),
        HM.set(new Key("2", 2), 2),
        HM.set(new Key("3", 3), 3),
        HM.set(new Key("4", 4), 4),
      )
      expect(H.unknown(map)).not.toEqual(H.unknown(otherMap))
    })
  })

  describe("Equatable", () => {
    it("should return true for a HashMap with the same keys and values", () => {
      const otherMap = pipe(
        HM.makeDefault<Key, number>(),
        HM.set(new Key("1", 1), 1),
        HM.set(new Key("2", 2), 2),
        HM.set(new Key("3", 3), 3),
      )
      expect(strictEqualsUnknown(map, otherMap)).toEqual(true)
    })
    it("should return true for a HashMap with different keys and values", () => {
      const otherMap = pipe(
        HM.makeDefault<Key, number>(),
        HM.set(new Key("1", 1), 1),
        HM.set(new Key("2", 2), 2),
        HM.set(new Key("3", 3), 3),
        HM.set(new Key("4", 4), 4),
      )
      expect(strictEqualsUnknown(map, otherMap)).toEqual(false)
    })
  })
})
